# coding=utf-8
import copy

import simplejson
import tornado
from cigdl_valuation_constants.cigdl_valuation_definitions import HousesTypes, ValuationDefaults, SaleRentValues, \
    SubTypeValues, GlazingValues, HeatingTypeValues, ValuationOutputs
from constants.hl_constants import ApplicationPreferences
from frontlib.frontend_constants import SaleRentTypes
from frontlib.hl_shared_constants import HouserLoopAuthTypes, ValuationServiceConstants, AppraisalDBFieldNames, \
    HLServicesUrlTypes, ReportTypes, PortfolioValuationTypes, GeoServiceConstants
from handlers.access_control_handler import AccessControlHandler
from handlers.base_handler import BaseHandler
from handlers.hl_tracking_types import HouserloopTrackingTypes
from handlers.liser_valuation_handlers import LiserValuationHandler
from handlers.reports_handler import ReportFormHandler
from handlers.template_types import TemplateTypes
from services.portfolio_services import PortfolioValuationService
from utils.csv_service import CsvService
from utils.frontend_utils import FrontendUtils

class ValuationCSVTemplateDownloadHandler(LiserValuationHandler):
    """
    allows the user to download a CSV template file that has example property values with the correct
    fieldnames that HouserLOOP accepts
    """
    @AccessControlHandler.admin_auth_for_agency()
    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self,agency_id):
        ftype = self.get_argument(PortfolioValuationTypes.FTYPE)
        table_keys = self.obligatory_liser_valuation_fields
        appr_arr = []
        appr_obj = {
            HousesTypes.ADDRESS: "150 Rue Cents, 1319, Cents, Luxembourg",
            HousesTypes.COMMUNE:"luxembourg",
            HousesTypes.ORIGTYPE: HousesTypes.APARTMENT,
            HousesTypes.SALE_RENT: SaleRentValues.BUY,
            HousesTypes.LIVINGSIZE: "180",
            HousesTypes.ENERGY_CLASS: "D",
            HousesTypes.YEAR_BUILT_NUM: "2010"
        }
        yield self.add_user_tracking(HouserloopTrackingTypes.VALUATION_CSV_TEMPLATE_DOWNLOAD_GET,"agency_id = {}".format(agency_id))

        if ftype == PortfolioValuationTypes.MINIMAL:
            appr_arr.append(appr_obj)
        elif ftype == PortfolioValuationTypes.ALL:
            table_keys = []

            appr_obj = {
                HousesTypes.ADDRESS: "150 Rue Cents, 1319, Cents, Luxembourg",
                HousesTypes.COMMUNE: "luxembourg",
                HousesTypes.SUBURB:"cents",
                HousesTypes.ORIGTYPE:HousesTypes.APARTMENT,
                HousesTypes.SUBTYPE:SubTypeValues.DUPLEX,
                HousesTypes.SALE_RENT:SaleRentValues.BUY,
                HousesTypes.YEAR_BUILT_NUM:"2010",
                HousesTypes.EMPHYTEUTIC_LEASE:"",
                HousesTypes.CHARGE:"100",
                HousesTypes.BEDROOMS:"2",
                HousesTypes.ENERGY_CLASS:"D",
                HousesTypes.ENERGY_EFFICIENCY:"D",
                HousesTypes.FLOOR_NUM:"3",
                HousesTypes.BATHROOMS:"2",
                HousesTypes.TOILETS:"2",
                HousesTypes.GARAGESNUM:"1",
                HousesTypes.PARKINGS_NUM:"0",
                HousesTypes.TERRACE_NUM:"1",
                HousesTypes.BALCONY_NUM:"1",
                HousesTypes.GLAZING:GlazingValues.DOUBLE,
                HousesTypes.HEATING_TYPE:HeatingTypeValues.GAS,
                HousesTypes.LIVINGSIZE:"180",
                HousesTypes.TERRACE:"20",
                HousesTypes.BALCONY:"5",
                HousesTypes.ATTIC:"",
                HousesTypes.BASEMENT:"",
                HousesTypes.GARAGE:"18",
                HousesTypes.NUM_STORIES:"4",
            }
            table_keys = self._liser_factual_fields
            appr_arr.append(appr_obj)

        use_filename = 'houserloop_portfolio_valuation_template_{}.csv'.format(ftype)
        # self.write_csv_rows_to_file(use_filename, appr_arr, table_keys)
        self.set_header('Content-Type', 'text/csv')
        self.set_header('content-Disposition', 'attachment; filename={}'.format(use_filename))
        header_arr = []
        for header_key in table_keys:
            header_arr.append(self.user_locale.translate('"{}"'.format(header_key)))
        #header_str = ','.join(['"{}"'.format(v) for v in table_keys])
        header_str = ','.join(header_arr)
        self.write('{}\r\n'.format(header_str))  # File header
        for line in appr_arr:
            line_str = ','.join(['"{}"'.format(line.get(key)) for key in table_keys])
            line_str = line_str.replace('&nbsp;',' ')
            line_str = line_str.replace('None', '')
            self.write(line_str + '\r\n')
            yield self.flush()

class PortfolioValuationPageHandler(LiserValuationHandler):
    """
    show's the user a page with an input form for generating valuations for a CSV spreadsheet of property objects
    User's can choose options such as:
        PortfolioValuationTypes.RETURN_CSV:displaying output as HTML or CSV
        PortfolioValuationTypes.CLEAN_ADDRESSES: resolve the address column using the houserloop geo_lookup service
        ApplicationPreferences.GEO_OBSCURE_ADDRESS: change specific street numbers to street number range in the address
    """
    @property
    def portfolio_limit(self):
        prefs = self.app_pref_obj or {}
        #self.LOGGER.debug("prefs = {}".format(prefs))
        portfolio = prefs.get(ApplicationPreferences.PORTFOLIO) or {}
        portfolio_row_limit = portfolio.get(ApplicationPreferences.PORTFOLIO_ROW_LIMIT) or -1
        portfolio_row_limit = FrontendUtils.to_num(portfolio_row_limit)
        return portfolio_row_limit

    @tornado.gen.coroutine
    def before_get_post(self, agency_id):
        self.template_kwargs = yield self.get_default_template_kwargs()
        self.template_kwargs[AppraisalDBFieldNames.AGENCY_ID] = agency_id
        self.template_kwargs[PortfolioValuationTypes.RETURN_CSV] = self.get_optional_arg(
            PortfolioValuationTypes.RETURN_CSV)
        self.template_kwargs[PortfolioValuationTypes.CLEAN_ADDRESSES] = self.get_optional_arg(
            PortfolioValuationTypes.CLEAN_ADDRESSES) or 1
        self.template_kwargs[ApplicationPreferences.GEO_OBSCURE_ADDRESS] = self.get_optional_arg(
            ApplicationPreferences.GEO_OBSCURE_ADDRESS)

        return self.template_kwargs

    @AccessControlHandler.admin_auth_for_agency()
    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self,agency_id=None):
        self.template_kwargs = yield self.before_get_post(agency_id)
        self.template_kwargs[AppraisalDBFieldNames.AGENCY_ID] = agency_id

        yield self.add_user_tracking(HouserloopTrackingTypes.PORTFOLIO_VALUATION_PAGE_GET,
                                     "agency_id = {}".format(agency_id))
        return self.render(TemplateTypes.PORTFOLIO_VALUATION_PAGE, **self.template_kwargs)

class LiserPortfolioCSVHandler(PortfolioValuationPageHandler):
    """
    returns LISER valuations for input rows with HTML output
    """
    _csv_service_inst = None
    @property
    def csv_service_inst(self):
        if not self._csv_service_inst:
            self._csv_service_inst = CsvService()
        return self._csv_service_inst

    _portfolio_valuation_service_inst = None
    @property
    def portfolio_valuation_service_inst(self) -> PortfolioValuationService:
        if not self._portfolio_valuation_service_inst:
            self._portfolio_valuation_service_inst = PortfolioValuationService(self.LOGGER,self)
        return self._portfolio_valuation_service_inst
    
    @tornado.gen.coroutine
    def get_liser_valuation_for_row(self, row):
        body = {
            ValuationServiceConstants.RESET: 1,
            ValuationServiceConstants.OVERWRITE_EXISTING: 1,
            AppraisalDBFieldNames.USER_REPORT_SEQ: -1,
            ValuationServiceConstants.CHECK_INPUTS: True,
            HLServicesUrlTypes.LISER_VALUATION_API_KEY: self.application.liser_valuation_service_key,
            # ValuationServiceConstants.ARGUMENTS: self.get_necessary_arguments()
        }
        body.update(
            {ValuationServiceConstants.APPR_OBJ: simplejson.dumps(row,
                                                                  default=self.date_handler)})
        resource = ValuationServiceConstants.VALUATION_RESOURCE

        output = yield self.get_valuation_response(body, resource, as_json=True)

        return output

    def bring_to_front(self,table_keys,key):
        if key in table_keys:
            table_keys.insert(0, table_keys.pop(table_keys.index(key)))

    def list_dict_contains_key(self,use_list,key):
        for list_obj in use_list:
            if list_obj.get(key):
                return True
        return False

    def filter_none_list_keys(self,use_list_keys,use_list):
        ret_list = []
        for key in use_list_keys:
            if self.list_dict_contains_key(use_list,key):
                ret_list.append(key)
        return ret_list
    
    @tornado.gen.coroutine
    def get_csv_valuation_rows(self, csv_reader, liser_valuation_col_key,override_energy_class=None,in_energy_class_arr=None,renovation_year_override=None):

        # get the headers of elements that have values and store in table_keys_obj
        rows_arr, table_keys_obj = self.portfolio_valuation_service_inst.get_rows_arr_from_csv_rows(csv_reader)

        table_keys_obj[HousesTypes.ORIGTYPE] = HousesTypes.APARTMENT
        table_keys_obj[ValuationOutputs.FINAL_PROPERTY_PRICE] = True

        if HousesTypes.SALE_RENT not in table_keys_obj:
            table_keys_obj[HousesTypes.SALE_RENT] = SaleRentValues.BUY

        clean_addresses = self.get_optional_arg(PortfolioValuationTypes.CLEAN_ADDRESSES)
        geo_obscure_address = self.get_optional_arg(ApplicationPreferences.GEO_OBSCURE_ADDRESS) or GeoServiceConstants.GEO_OBSCURE_NONE_LEVEL
        if geo_obscure_address:
            geo_obscure_address = GeoServiceConstants.GEO_OBSCURE_HOUSE_NUM_LEVEL
        appr_arr = []
        use_rows_arr = rows_arr

        if self.portfolio_limit > 0:
            if len(rows_arr) > self.portfolio_limit:
                use_rows_arr = rows_arr[:self.portfolio_limit]
        dont_include_fields = self.get_optional_arg(PortfolioValuationTypes.DONT_INCLUDE_FIELDS,None)
        dont_include_fields_arr = []
        if dont_include_fields:
            dont_include_fields_arr = [x.strip() for x in dont_include_fields.split(',')]

        self.template_kwargs[PortfolioValuationTypes.DONT_INCLUDE_FIELDS] = dont_include_fields
        for i, row in enumerate(use_rows_arr):
            energy_class_arr = list(in_energy_class_arr)
            if override_energy_class:
                row[HousesTypes.ENERGY_CLASS] = override_energy_class

            #first handle the case with the normal energy classes
            yield self.portfolio_valuation_service_inst.clean_row_for_valuation(row, clean_addresses,
                                                                                geo_obscure_address,renovation_year_override=renovation_year_override)

            appr_obj = dict(row)
            output = yield self.get_liser_valuation_for_row(row)
            self.portfolio_valuation_service_inst.populate_appr_obj_with_valuation(appr_obj, output,
                                                                                   liser_valuation_col_key)
            if row.get(HousesTypes.ENERGY_CLASS) not in energy_class_arr:
                energy_class_arr.append(row.get(HousesTypes.ENERGY_CLASS))

            for energy_class in energy_class_arr:
                temp_row = copy.deepcopy(row)
                if dont_include_fields_arr:
                    for key in dont_include_fields_arr:
                        if key in temp_row:
                            del temp_row[key]
                if HousesTypes.ENERGY_CLASS_NUM in temp_row:
                    del temp_row[HousesTypes.ENERGY_CLASS_NUM]
                    temp_row[HousesTypes.ENERGY_CLASS] = energy_class
                yield self.portfolio_valuation_service_inst.clean_row_for_valuation(temp_row, clean_addresses,
                                                                                    geo_obscure_address,renovation_year_override=renovation_year_override)

                temp_appr_obj = copy.deepcopy(temp_row)
                output = yield self.get_liser_valuation_for_row(temp_row)
                self.portfolio_valuation_service_inst.populate_appr_obj_with_valuation(temp_appr_obj, output,
                                                                                       liser_valuation_col_key)

                energy_class_key = "liser_valuation_energy_class_"+energy_class
                final_property_price = temp_appr_obj.get(ValuationOutputs.FINAL_PROPERTY_PRICE)
                appr_obj[energy_class_key] = FrontendUtils.add_euro(final_property_price)

            appr_arr.append(appr_obj)


        extra_keys = [
            liser_valuation_col_key,
            "final_property_std",
            'price_display',
            'valuation_diff',
            'valuation_diff_perc',
            'valuation_diff_display',
            'liser_submarket_id',
            'accesstime_luxcity'
        ]
        table_keys = []

        table_keys.extend(table_keys_obj.keys())
        table_keys.extend(extra_keys)

        self.bring_to_front(table_keys, 'suburb')
        self.bring_to_front(table_keys, 'commune')
        self.bring_to_front(table_keys, 'valuation_diff_display')

        self.bring_to_front(table_keys, "final_property_std")
        self.bring_to_front(table_keys, 'valuation_diff_perc')
        self.bring_to_front(table_keys, liser_valuation_col_key)
        self.bring_to_front(table_keys, 'price_display')
        self.bring_to_front(table_keys, 'deactivationprice')
        self.bring_to_front(table_keys, 'orig_price')
        self.bring_to_front(table_keys, 'liser_submarket_id')
        self.bring_to_front(table_keys, 'accesstime_luxcity')
        self.bring_to_front(table_keys, 'address')
        table_keys = [
            'orig_address',
            'address',
            'orig_suburb',
            'suburb',
            'orig_commune',
            'commune',
            #'accesstime_luxcity',
            'livingsize',
            'garage',
            'terrace',
            'balcony',
            'garden',
            liser_valuation_col_key,
        ]
        table_keys = self.filter_none_list_keys(table_keys,appr_arr)

        return appr_arr, table_keys,table_keys_obj



    @AccessControlHandler.admin_auth_for_agency()
    @tornado.gen.coroutine
    @tornado.web.authenticated
    def post(self,agency_id):
        """
        takes a CSV input file, calculates a LISER valuation for each row
        and returns a HTML table with rows
        """
        self.template_kwargs = yield self.before_get_post(agency_id)
        return_csv = self.get_optional_arg(PortfolioValuationTypes.RETURN_CSV)
        clean_addresses = self.get_optional_arg(
            PortfolioValuationTypes.CLEAN_ADDRESSES) or 0
        self.template_kwargs[PortfolioValuationTypes.CLEAN_ADDRESSES] = clean_addresses
        yield self.add_user_tracking(HouserloopTrackingTypes.LISER_PORTFOLIO_CSV_POST,
                                     "clean_addresses = {} return_csv = {} agency_id = {}".format(clean_addresses,return_csv,agency_id))


        # accept input file from
        csv_reader,filename = self.csv_service_inst.get_uploaded_csv_reader(self)


        if not csv_reader:
            return self.write("Not a valid csv file")

        liser_valuation_col_key = 'LISER valuation'
        override_energy_class = self.get_optional_arg(PortfolioValuationTypes.OVERRIDE_ENERGY_CLASS,None)

        energy_class_list_str = self.get_optional_arg(PortfolioValuationTypes.ENERGY_CLASS_LIST, None)
        self.template_kwargs[PortfolioValuationTypes.ENERGY_CLASS_LIST] = energy_class_list_str

        renovation_year_override = self.get_optional_arg(PortfolioValuationTypes.RENOVATION_YEAR_OVERRIDE, None)
        self.template_kwargs[PortfolioValuationTypes.RENOVATION_YEAR_OVERRIDE] = renovation_year_override

        energy_class_arr = []
        if energy_class_list_str:
            energy_class_arr = energy_class_list_str.split(',')
        self.template_kwargs[PortfolioValuationTypes.OVERRIDE_ENERGY_CLASS] = override_energy_class

        appr_arr, table_keys, orig_table_keys_obj = yield self.get_csv_valuation_rows( csv_reader, liser_valuation_col_key,override_energy_class,energy_class_arr,renovation_year_override=renovation_year_override)

        orig_table_keys = csv_reader.fieldnames
        successfully_processed = 0
        not_processed = 0
        for appr_obj in appr_arr:
            if appr_obj.get(liser_valuation_col_key):
                successfully_processed += 1
            else:
                not_processed += 1
        status_message = ""
        if successfully_processed:
            status_message += self.user_locale.translate("Successfully processed {} rows".format(successfully_processed))
        status_message += "<br>"
        if not_processed:
            status_message += self.user_locale.translate("Couldn't process {} rows".format(not_processed))

        self.template_kwargs['status_message'] = status_message
        self.template_kwargs['rows_arr'] = appr_arr
        self.template_kwargs['appr_arr'] = appr_arr


        self.template_kwargs['table_keys'] = table_keys

        em_cols = [
            liser_valuation_col_key
        ]
        self.template_kwargs['em_cols'] = em_cols

        use_template = TemplateTypes.TABLE_FROM_ROWS_ARR
        render_key_map = {
            "valuation-results":use_template
        }

        self.template_kwargs[AppraisalDBFieldNames.AGENCY_ID] = agency_id

        header_arr = []
        extra_keys = [
            liser_valuation_col_key,
            #"orig_" + HousesTypes.YEAR_BUILT_NUM,
        ]
        for energy_class in energy_class_arr:
            extra_keys.append("liser_valuation_energy_class_" + energy_class)

        for header_key in orig_table_keys:
            header_arr.append(self.user_locale.translate('"{}"'.format(header_key)))
        for header_key in extra_keys:
            header_arr.append(self.user_locale.translate('"{}"'.format(header_key)))

        all_keys = []
        all_keys.extend(orig_table_keys)
        all_keys.extend(extra_keys)

        self.template_kwargs['table_keys'] = all_keys
        if return_csv:

            use_filename = 'houserloop_portfolio_valuation_csv_output.csv'
            # self.write_csv_rows_to_file(use_filename, appr_arr, table_keys)
            self.set_header('Content-Type', 'text/csv;charset=UTF-8')
            self.set_header('Content-Encoding', 'UTF-8')
            self.set_cookie("fileDownloadToken", self.get_argument('fileDownloadToken'))
            self.set_header('content-Disposition', 'attachment; filename={}'.format(use_filename))

            #header_str = ','.join(['"{}"'.format(v) for v in table_keys])
            header_str = ','.join(header_arr)
            self.write('{}\r\n'.format(header_str))  # File header
            for line in appr_arr:
                line_str = ','.join(['"{}"'.format(line.get(key)) for key in all_keys])
                line_str = line_str.replace('&nbsp;',' ')
                line_str = line_str.replace('None', '')
                self.write(line_str + '\r\n')
                yield self.flush()
        else:
            return self.render(TemplateTypes.PORTFOLIO_VALUATION_PAGE, **self.template_kwargs)


class LiserPortfolioCSVOutputHandler(LiserPortfolioCSVHandler):
    """
    returns LISER valuations for input rows in a CSV file
    """
    @AccessControlHandler.admin_auth_for_agency()
    @tornado.gen.coroutine
    @tornado.web.authenticated
    def post(self,agency_id):
        self.template_kwargs = yield self.get_default_template_kwargs()
        self.template_kwargs[AppraisalDBFieldNames.AGENCY_ID] = agency_id

        yield self.add_user_tracking(HouserloopTrackingTypes.LISER_PORTFOLIO_CSV_OUTPUT_POST,
                                     "agency_id = {}".format(agency_id))

        # accept input file from
        csv_reader, filename = self.csv_service_inst.get_uploaded_csv_reader(self)
        if not csv_reader:
            return self.write("Not a valid csv file")

        liser_valuation_col_key = 'LISER valuation'
        appr_arr, table_keys, orig_table_keys_obj = yield self.get_csv_valuation_rows(csv_reader, liser_valuation_col_key)

        use_filename = 'houserloop_portfolio_valuation_csv_output.csv'
        #self.write_csv_rows_to_file(use_filename, appr_arr, table_keys)
        self.set_header('Content-Type', 'text/csv')
        self.set_header('content-Disposition', 'attachment; filename={}'.format(use_filename))
        header_str = ','.join(['"{}"'.format(v) for v in table_keys])
        self.write('{}\r\n'.format(header_str))  # File header
        for line in appr_arr:
            line_str = ','.join(['"{}"'.format(line.get(key)) for key in table_keys])
            self.write(line_str + '\r\n')
            yield self.flush()