This repository contains 3 randomly selected files from the Houser/HouserLOOP
project. They are not intended as a packaged application, only a showcase of
source code written by Kevin Colgan.

-   **EventsTransformer.java** is a Java source file that forms part of a SOLR
    indexation project which takes property objects, their duplicates and their
    events and consolidates them into a cleaned property object row. It is a
    custom Transformer implementation, building on the base Solr Transformer
    class. The SOLR search platform is used in the HouserLOOP and houser.lu
    projects

-   **houserloop-graphs.js** is a Javascript file that uses the D3 vector
    graphics library to draw graphs for properties. There are 3 graphs that can
    be drawn: an events graph showing price changes, an aggregate price events
    graph which shows price events in one graph along with median values, and a
    price range graph which shows the distribution of property prices within a
    range.

-   **portfolio_handlers.py** is a Python source file for use in the HouserLOOP
    tornado application. It accepts input from a form and calculates valuations
    for a comma separated value file of property objects, providing output in
    either HTML or CSV file format
