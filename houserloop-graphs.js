//Wrapping Long Labels example from here: http://bl.ocks.org/mbostock/7555321
function wrapText(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    ////////console.info("wrapText dy = "+dy+" y = "+y+" lineNumber = "+lineNumber+" lineHeight = "+lineHeight);
    //http://stackoverflow.com/questions/8108184/why-does-jshint-not-recognize-an-assignment-as-an-expression
    while (!!(word = words.pop())) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

function setGraphInitFromAnalytics(_obj,analytics_obj)
{
    /*
    the graph will show price changes against time-on-market relative to the first 'created' event
    */
    _obj.xKey = 'tom_rel_to_min_ms';
    _obj.yKey = 'eventprice';
    //////////console.info("setGraphInitFromAnalytics analytics_obj = "+analytics_obj);
    if(analytics_obj !== undefined && analytics_obj !== null && !$.isEmptyObject(analytics_obj))
    {
        _obj.min_y = analytics_obj[hl.AnalyticsTypes.MIN_ASKINGPRICE];
        _obj.max_y = analytics_obj[hl.AnalyticsTypes.MAX_ASKINGPRICE];
        _obj.min_deselected_y = analytics_obj[hl.AnalyticsTypes.MIN_DESELECTED_ASKINGPRICE];
        _obj.max_deselected_y = analytics_obj[hl.AnalyticsTypes.MAX_DESELECTED_ASKINGPRICE];
        _obj.median_y = analytics_obj[hl.AnalyticsTypes.MEDIAN_ASKINGPRICE];
        _obj.mean_y = analytics_obj[hl.AnalyticsTypes.MEAN_ASKINGPRICE];
        _obj.min_x = 0;
        _obj.max_x = analytics_obj[hl.AnalyticsTypes.MAX_TOM];
        _obj.max_deselected_x = analytics_obj[hl.AnalyticsTypes.MAX_DESELECTED_TOM];
        _obj.median_x = analytics_obj[hl.AnalyticsTypes.MEDIAN_TOM];
        _obj.mean_x = analytics_obj[hl.AnalyticsTypes.MEAN_TOM];
        _obj.analyticsYLinesArr = [hl.AnalyticsTypes.MEDIAN_ASKINGPRICE];
        _obj.analyticsXLinesArr = [hl.AnalyticsTypes.MEDIAN_TOM];
    }

}

/*
Main class constructor for adding an events graph based on the analytics object to the jquery containerSelector
*/
function EventsGraph(containerSelector,analytics_obj,group_id,section_type){

    this.containerSelector = containerSelector;
    this.analytics_obj = analytics_obj;
    this.group_id = group_id;
    //section_type is active, inactive or deselected
    this.section_type = section_type;

    setGraphInitFromAnalytics(this,analytics_obj);

    this.leftMargin = 70;
    this.rightMargin = 46;
    this.topMargin = 20;
    this.bottomMargin = 30;
    this.xAxisLabelDy = "2.3em";
}

EventsGraph.prototype = {
    constructor: EventsGraph,
    setWidthAndHeight:function(){
        /*
        sets the width and heights so that the graph can fit in a HTML div with
        a margin
        */
        this.containerWidth = 300;
        if(this.defaultWidth !== undefined)
        {
            this.containerWidth = this.defaultWidth;
        }
        if($(this.containerSelector).parent().width() > 0)
        {
            this.containerWidth = $(this.containerSelector).parent().width();
        }
        window.eventGraphWidth = this.containerWidth;

        this.containerHeight = 150;

        if($(this.containerSelector).parent().height() > 50)
        {
            this.containerHeight = $(this.containerSelector).parent().height();
        }
        var maxHeight = 160;
        if(this.defaultMinHeight !== undefined)
        {
            maxHeight = this.defaultMinHeight;
        }

        this.containerHeight = Math.min(this.containerHeight,maxHeight);

        //this needs to be big enough for the price to show

        this.focusMargin = {top: this.topMargin, right: this.rightMargin, bottom: this.bottomMargin, left: this.leftMargin};
        this.margin = this.focusMargin;

        this.focusHeight = this.containerHeight-this.focusMargin.top-this.focusMargin.bottom;
        this.width = this.containerWidth - this.margin.left - this.margin.right;
        this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    },
    getMaxYDomain:function()
    {
        return this.max_y;
    },
    setAxisDomains:function()
    {
        var maxYValues = this.max_y;
        var minYValues = this.min_y;
        //the deselected graph could be bigger than the existing one, so to show all values we have to use a bigger range
        if(this.section_type == hl.ReportTypes.DESELECTED)
        {
            maxYValues = Math.max(this.max_deselected_y,maxYValues);
            minYValues = Math.min(this.min_deselected_y,minYValues);
        }

        var percMargin = 0;
        this.minYValues = minYValues;
        this.minYDomain = minYValues-0.22*minYValues;
        this.maxYDomain = this.getMaxYDomain();

        this.minXDomain = this.min_x;
        this.maxXDomain = this.max_x;

        if(this.section_type == hl.ReportTypes.DESELECTED)
        {
            this.maxXDomain = Math.max(this.max_deselected_x,this.maxXDomain);
        }

        var x = d3.scale.linear()
        .range([0, this.width]);

        var y = d3.scale.linear()
            .range([this.height, 0]);


        x.domain([this.minXDomain,this.maxXDomain]);
        y.domain([this.minYDomain, this.maxYDomain]).nice();
        this.x = x;
        this.y = y;

    },
    addMarkerDefinitions:function(){
        //example of using D3 markers for line endings http://bl.ocks.org/dustinlarimer/5888271
        var suffix = $(this.containerSelector).closest('.tab-pane').attr('id');

        /*jshint -W075  */
        var data = [
            { id: 0, name: 'circle',id: 'circle'+"_"+suffix, path: 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0', viewbox: '-6 -6 12 12' },
            { id: 1, name: 'square',id: 'square'+"_"+suffix, path: 'M 0,0 m -5,-5 L 5,-5 L 5,5 L -5,5 Z', viewbox: '-5 -5 10 10' },
            { id: 2, name: 'arrow',id: 'arrow'+"_"+suffix, path: 'M 0,0 m -5,-5 L 5,0 L -5,5 Z', viewbox: '-5 -5 10 10' },
            { id: 2, name: 'uparrow',id: 'uparrow'+"_"+suffix, path: 'M 0,0 m -5,-5 L 5,0 L -5,5 Z', viewbox: '-5 -5 10 10', orient:270},
            { id: 3, name: 'linearrow',id: 'linearrow'+"_"+suffix, path: 'M 0,0 m -5,-5 L 5,0 L -5,5 Z', viewbox: '-5 -5 10 10' },
            { id: 4, name: 'stub',id: 'stub'+"_"+suffix, path: 'M 0,0 m -1,-5 L 1,-5 L 1,5 L -1,5 Z', viewbox: '-1 -5 2 10' }
        ];
        /*jshint +W075  */
        var defs = this.focus.append("defs")
            .attr('class',"defs-"+suffix);
        //console.debug("drawFromJson arr marker className = "+className(this));
        ////////console.info("addMarkerDefinitions var useClassName = className(this);");
        var useClassName = className(this);
        var classRef = this;
        classRef.markerMap = {};

        var markerId = function(d){
          var marker_name = '';
          if(d.name !== undefined)
          {
              classRef.markerMap[d.name] = d.id;
              marker_name = d.id;
          }
          //////console.info("marker_name= "+marker_name);
          return marker_name;
        };

        var marker = defs.selectAll('marker')
            .data(data)
            .enter()
            .append('svg:marker')
              .attr('id', markerId)
              .attr('markerHeight', 3.2)
              .attr('markerWidth', 3.2)
              .attr('markerUnits', 'strokeWidth')
              .attr('orient', function(d){
                  var orient = 'auto';
                  if(d.orient !== undefined)
                  {
                      orient = d.orient;
                  }
                  return orient;
                })
              .attr('refX', 0)
              .attr('refY', 0)
              .attr('class',markerId)
              .attr("markerUnits","strokeWidth")
              .attr('viewBox', function(d){
                  var vBox = null;
                  if(d.viewbox !== undefined)
                  {
                      vBox = d.viewbox;
                  }
                  return vBox;
                })
              .append('svg:path')
                .attr('d', function(d){
                    var d_path = null;
                    if(d.path !== undefined)
                    {
                        d_path = d.path;
                    }
                    return d_path;
                })
                ;


        //http://tutorials.jenkov.com/svg/marker-element.html#referencing-markers-from-a-path
        //http://stackoverflow.com/questions/26671932/d3js-display-arrow-at-the-end-of-each-axis
        //https://www.w3.org/TR/SVG/painting.html#MarkerElementRefXAttribute
    },
    drawAxes:function(){
        var xAxis = d3.svg.axis()
            .scale(this.x)
            .orient("bottom")
            .ticks(0)
            .tickFormat("")
            .tickSize(0);

        var yAxis = d3.svg.axis()
            .scale(this.y)
            .orient("left")
            .ticks(0)
            .tickFormat("")
            .tickSize(0)
        ;
        var xa = this.focus.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + this.height + ")")
          //.style("dominant-baseline", "central")
          .call(xAxis);

        var ya = this.focus.append("g")
          .attr("class", "y axis")
          .call(yAxis);

        ya.append("text")
          .attr("y", -10)
          .attr("x", -2)
          .attr("dy", "-0.4em")
          .style("text-anchor", "end")
          .text(hl_utils.getTranslationLabel("Price(€)"));

        xa.append("text")
          .attr("y", 0)
          .attr("x", (this.x(this.max_x)-this.x(this.min_x))/2)
          .attr("dy", this.xAxisLabelDy)
          .style("text-anchor", "middle")
          .text(hl_utils.getTranslationLabel("Time on market"));

        this.drawAxisArrows(xa,ya);

        this.xAxis = xAxis;
        this.yAxis = yAxis;
    },
    drawGraph:function(){
        this.setWidthAndHeight();
        this.setAxisDomains();

        var eg = this;
        var x = eg.x;
        var y = eg.y;
        this.line = d3.svg.line()
            .interpolate("step-after")
            .x(function(d) {
                var retVal = x(d[eg.xKey])+x(eg.min_x);
                return retVal;
            })
            .y(function(d) {
                var retVal = y(d[eg.yKey]);
                return retVal;
            });


        var svgWidth = this.width + this.margin.left + this.margin.right;
        var svgHeight = this.height + this.margin.top + this.margin.bottom;
        d3.select(this.containerSelector).select('svg').remove();
        var svg = d3.select(this.containerSelector).append("svg")
            .attr("width", svgWidth)
            .attr("height", svgHeight);

        var focus = svg.append("g")
                .attr('id','events_focus')
                .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
        this.svg = svg;
        this.focus = focus;

        this.addMarkerDefinitions();

    },
    dotFilter:function(d, i)
    {
        var retVal = true;
        if("event_type" in d && d.event_type === 'deactivationprice')
        {
            retVal = true;
        }
        return retVal;
    },
    addCircles:function(data)
    {

        var eg = this;
        var focusCircles;
        var dotContainer;
        var focus = this.focus;
        var x = this.x;
        var y = this.y;
        //console.debug("drawFromJson focus.append('g').attr('class'");
        if(focus.selectAll('.dot-container').empty())
        {
            focus.append("g").attr('class','dot-container');
        }
        dotContainer = focus.select('.dot-container');

        focusCircles = dotContainer.selectAll(".dot")
              .data(data.filter(eg.dotFilter));

        focusCircles.enter().append("circle");
        //need to remove the circles whenever we clear the graph
        focusCircles.exit()
            .attr("r", 0)
            .remove();

        focusCircles
            .attr("id", function (d) { return "focusCircle_"+String(d.group_id); })
            .attr("value", function (d) { return String(d.group_id); })
            .attr("cx", function (d) { return x(d[eg.xKey]); })
            .attr("cy", function (d) { return y(d[eg.yKey]); })
            .attr("class", function(d) { return 'dot inactive';})
            .attr("r", 0)
            ;

        this.focusCircles = focusCircles;
        this.dotContainer = dotContainer;

    },
    drawAnalyticsLine:function(lineKey,valArr)
    {
        var eg = this;
        var obj1 = {};
        obj1[this.xKey] = valArr[0];
        obj1[this.yKey] = valArr[1];
        var obj2 = {};
        obj2[this.xKey] = valArr[2];
        obj2[this.yKey] = valArr[3];
        var useData = [
            obj1,
            obj2
        ];
        //console.debug("drawFromJson var path = this.focus.append");
        var path = this.focus.append("path")
          .datum(useData)
          .attr("id",lineKey)
          .attr("class", "line "+lineKey)
          .attr("d", eg.line);

    },
    addLineText:function(text_path_id,text_path_val,useClass,dominant_baseline,dy_offset,yPos)
    {

        var eg = this;
        var dy = this.getDyFromBaseline(dominant_baseline,dy_offset);
        if(yPos === undefined)
        {
            yPos = 0;
        }
        //console.debug("addLineText yPos = "+yPos);
        eg.focus.append("text")
            .attr("id", text_path_id+"_label")
            .attr("dy",yPos)
            .attr("class",useClass)

          .append("textPath")
            //.attr("dominant-baseline", dominant_baseline)
            //.attr("xlink:href", "#"+text_path_id)
            .text(text_path_val);
    },
    drawMaxMinArea:function()
    {
        var _this = this;
        var eg = this;
        var x = this.x;
        var y = this.y;

        eg.drawAnalyticsLine("min_y",[this.min_x,this.min_y,this.max_x,this.min_y]);
        eg.drawAnalyticsLine("max_y",[this.min_x,this.max_y,this.max_x,this.max_y]);
        eg.drawAnalyticsLine("max_date",[this.max_x,this.minYDomain,this.max_x,this.max_y]);

        var max_y = this.max_y;
        eg.focus.append("text")
            .attr("id", "max_y")
            .attr("dy","-0.2em")
            .attr("class","max-price")
            .attr("x",function(d){
                var retVal = _this.x(0);
                return retVal;
            })
            .attr("y",function(d){
                var retVal = _this.y(_this.max_y);
                return retVal;
            })
            .text(hl_utils.getTranslationLabel("max. %(val)s",formatCurrency(_this.max_y)));

        var min_y = this.min_y;
        eg.focus.append("text")
            .attr("id", "min_y")
            .attr("dy","1em")
            .attr("class","min-price")
            .attr("dx","0.2em")
            .attr("x",function(d){
                var retVal = _this.x(0);
                return retVal;
            })
            .attr("y",function(d){
                var retVal = _this.y(_this.min_y);
                return retVal;
            })
            .text(hl_utils.getTranslationLabel("min. %(val)s",formatCurrency(_this.min_y)));
        //this.addLineText("max_y",formatCurrency(this.max_y),"max-price","no-change",0,y(this.max_y)-3);
        //this.addLineText("min_y",formatCurrency(this.min_y),"min-price","hanging",0,y(this.min_y)+11);

        var y_diff = this.max_y-this.min_y;

        this.focus.append("rect")
            .attr("class", "min-max-rect")
            .attr("x", _this.x(this.min_x))
            .attr("y", _this.y(this.max_y))
            .attr("width", _this.x(this.max_x-this.min_x))
            .attr("height", _this.y(_this.min_y) - _this.y(_this.max_y))
            ;

    },
    addBackgroundToText:function(textEl,textContainer,className,paddingX,paddingY)
    {
        if(this.hasValidBBox(textEl))
        {
            try
            {
                var clientRect = textEl[0][0].getBBox();
                textContainer.append("rect")
                    .attr("class",className)
                    .attr("x",clientRect.x-paddingX)
                    .attr("y",clientRect.y-paddingY)
                    .attr("width",clientRect.width+paddingX*2)
                    .attr("height",clientRect.height+paddingY*2)
                    ;
                textContainer.append(function() {
                  return textContainer.selectAll("text").remove().node();
                });
            }
            catch(e)
            {
                console.debug("ERROR: addBackgroundToText e = "+e);
            }
        }
    },
    hasValidBBox:function(inEl)
    {
        var retVal = false;
        if($(inEl).length > 0 && $(this.containerSelector).parents('.tab-pane.active').length > 0)
        {
            retVal = true;
        }
        retVal = true;
        return retVal;
    },
    addPriceAverageLabel:function(lineKey,xVal,yVal,dominant_baseline)
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        var useClass = lineKey+" text-label";
        var dy = this.getDyFromBaseline(dominant_baseline);

        var textContainer = eg.focus.append("g")
            .attr("class","price-analytics-text-container")
            .attr("transform","translate("+x(xVal)+","+y(yVal)+")");

        var yValCurrency = formatCurrency(yVal,"");
        var currencyVal = hl_utils.getTranslationLabel("%(val)s",yValCurrency);
        var medianLabel = hl_utils.getTranslationLabel("Median");
        var textEl = textContainer.append("text")
            .attr("id", lineKey+"_label_av")
            //.attr("dx","-0.2em")
            .attr("dy","0")
            .attr("class",useClass)
            //.attr("dominant-baseline", "no-change")
            //.text(hl_utils.getTranslationLabel("Median"))
            .text(medianLabel);
        textEl.append('tspan')
            .attr('class','value')
            .text(" "+currencyVal);

        textContainer.selectAll("text")
            .call(wrapText, this.leftMargin);

        var paddingX = 4;
        var paddingY = 1;
        var className = "price-analytics-text-bg";
        this.addBackgroundToText(textEl,textContainer,className,paddingX,paddingY);
        try
        {
            if(this.hasValidBBox(textContainer))
            {
                var textContainerClientRect = textContainer[0][0].getBBox();
                var newX = textContainerClientRect.x-textContainerClientRect.width+paddingX*2;
                textContainer
                    .attr("transform","translate("+newX+","+y(yVal)+")");
            }

        }
        catch(e)
        {
            console.debug("ERROR: addBackgroundToText e = "+e);
        }

    },
    getDyFromBaseline:function(baseline,offset)
    {
        var use_offset = 0;
        if(offset !== undefined)
        {
            use_offset = offset;
        }
        var dy = -0.3;
        if(baseline == "hanging")
        {
            dy = 1.1;
        }
        dy = dy+use_offset;
        return dy+"em";
    },
    addDateAverageLabel:function(av_val)
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        var useDateVal = av_val+eg.min_x;

        var useClass = "date-average-label";
        var useId = "date_average_label_"+this.group_id;
        var dy = this.getDyFromBaseline("hanging");
        var averageDateTextContainer = this.focus.append("g")
            .attr('class',"average-date-text-container")
            ;
        var textEl = averageDateTextContainer.append("text")
            .attr("id", useId)
            //.attr("dominant-baseline","hanging")
            .attr("class",useClass)
            .attr("x",function(d) {
                var retVal = x(useDateVal);
                return retVal;
            })
            .attr("y",function(d) {
                //var retVal = y(eg.minYDomain);
                var retVal = eg.height;
                return retVal;
            })
            .attr("dy",dy)
            .attr("dx","-0.2em")
            .text(hl_utils.getTranslationLabel("Median",hl_utils.formatMilliseconds(useDateVal)))
            ;
        textEl.append('tspan')
            .attr('class','value')
            .text(" "+hl_utils.formatMilliseconds(useDateVal));

        var paddingX = 5;
        var paddingY = 0;
        ////////console.info("textEl = "+textEl);
        this.addBackgroundToText(textEl,averageDateTextContainer,"average-date-bg",paddingX,paddingY);
    },
    drawAnalyticsLines:function()
    {
        var eg = this;
        var useArr = eg.analyticsYLinesArr;
        var i;
        var xLineKey;
        if(useArr instanceof Array)
        {
            //the price Av line(s)
            for(i=0;i<useArr.length;i++)
            {
                var lineKey = useArr[i];
                var lineVal = eg.analytics_obj[lineKey];
                xLineKey = eg.analyticsXLinesArr[i];
                var max_x = eg.analytics_obj[xLineKey];
                eg.drawAnalyticsLine(lineKey,[eg.min_x,lineVal,max_x,lineVal]);
                eg.addPriceAverageLabel(lineKey,eg.min_x,lineVal);
            }
        }

        useArr = eg.analyticsXLinesArr;
        if(useArr instanceof Array)
        {
            //the date Av line(s)
            for(i=0;i<useArr.length;i++)
            {
                xLineKey = useArr[i];
                var yLineKey = eg.analyticsYLinesArr[i];

                var xLineVal = eg.analytics_obj[xLineKey];
                var yLineVal = eg.analytics_obj[yLineKey];

                eg.drawAnalyticsLine(yLineKey,[xLineVal,eg.minYDomain,xLineVal,yLineVal]);
                eg.addDateAverageLabel(xLineVal);
            }
        }
    },
    addPriceLabel:function(text_val,useId,useClass,use_baseline,use_val,dx_offset,dy_offset)
    {

        var eg = this;
        var x = this.x;
        var y = this.y;
        if(use_baseline == "hanging")
        {
            dy_offset = dy_offset-0.6;
        }
        var dy = this.getDyFromBaseline(use_baseline,dy_offset);
        var use_dx_offset = 0;
        if(dx_offset !== undefined)
        {
            use_dx_offset = dx_offset;
        }
        var use_dx = use_dx_offset+"em";
        ////////////console.info("eg.xKey = "+eg.xKey);
        eg.focus.append("text")
            .attr("id", useId)
            //.attr("dominant-baseline",use_baseline)
            .attr("class",useClass)
            .attr("dy",dy)
            .attr("x",function(d) {
                var retVal = x(eg.min_x);
                if(use_val[eg.xKey] !== undefined)
                {
                    retVal += x(use_val[eg.xKey]);
                }
                return retVal;
            })
            .attr("y",function(d) {
                var retVal = 0;
                if(use_val[eg.yKey] !== undefined)
                {
                    retVal += y(use_val[eg.yKey]);
                }
                return retVal;
            })
            .attr("dx",use_dx)
            .text(text_val);

    },
    addPriceLabels:function(data,first_val,last_val)
    {

        var eg = this;
        var x = this.x;
        var y = this.y;
        var first_baseline = "no-change";
        var last_baseline = "no-change";
        var price_change = false;
        if(first_val[eg.yKey] > last_val[eg.yKey])
        {
            price_change = true;
            first_baseline = "no-change";
            last_baseline = "hanging";
        }
        else if(first_val[eg.yKey] < last_val[eg.yKey])
        {
            price_change = true;
            first_baseline = "hanging";
            last_baseline = "no-change";
        }

        var useClass = "price-start";
        if(!price_change)
        {
            useClass = "price-start hidden";
        }
        var useId = useClass+"_label_"+this.group_id;
        var priceTextVal = formatCurrency(first_val[eg.yKey]);

        var default_y_offset = 0.3;
        this.addPriceLabel(priceTextVal,useId,useClass,first_baseline,first_val,-0.4,default_y_offset);

        useClass = "price-end";
        useId = useClass+"_label_"+this.group_id;
        priceTextVal = formatCurrency(last_val[eg.yKey]);

        useClass = "date-end";
        useId = useClass+"_date_label_"+this.group_id;
        //add date to the end also
        priceTextVal = hl_utils.formatMilliseconds(last_val[eg.xKey]);
        var dy_offset = 1.6-default_y_offset;
        if(last_baseline == "hanging")
        {
            dy_offset = dy_offset-0.6;
        }

        var dy = this.getDyFromBaseline(last_baseline,dy_offset);
        var xPos = function(d) {
            var retVal = x(eg.min_x);
            if(last_val[eg.xKey] !== undefined)
            {
                retVal += x(last_val[eg.xKey]);
            }
            return retVal;
        };
        var yPos = function(d) {
            var retVal = 0;
            if(last_val[eg.yKey] !== undefined)
            {
                retVal += y(last_val[eg.yKey]);
            }
            return retVal;
        };

        var rect_id = "label_rect_"+eg.group_id;
        eg.focus.selectAll(".label-rect")
         .data([last_val])
       .enter().append("rect")
         .attr("id",rect_id)
         .attr("class","label-rect")
         .attr("stroke", "black")
         .attr("x", xPos)
         .attr("y", yPos)
         .attr("opacity",0)
         .attr("width", 1)
         .attr("height", 1)
            ;

    },
    addDateMaxLabel:function(data,first_val,last_val)
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        var first_baseline = "hanging";
        var last_baseline = "hanging";

        var useClass = "date-max";
        var useId = useClass+"_label_"+this.group_id;

        eg.focus.append("text")
            .attr("id", useId)
            //.attr("dominant-baseline","ideographic")
            .attr("class",useClass)
            .attr("dx","-0.1em")
            .attr("dy","-0.3em")
            .style("text-anchor", "middle")
            .attr("dy","1.1em")
            .attr("x",this.x(this.max_x))
            .attr("y",function(d) {
                //var retVal = y(eg.minYDomain);
                var retVal = eg.height;
                return retVal;
            })
            .text(hl_utils.getTranslationLabel("max. %(val)s",hl_utils.formatMilliseconds(eg.maxXDomain)));
    },
    addMinMaxDateLabel:function()
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        var first_baseline = "hanging";
        var last_baseline = "hanging";

        //this.addLineText("price_0",formatCurrency(first_val),"price-start",first_baseline);
        var useClass = "date-start";
        var useId = useClass+"_label_"+this.group_id;

        useClass = "date-max";
        useId = useClass+"_label_"+this.group_id;
        eg.focus.append("text")
            .attr("id", useId)
            //.attr("dominant-baseline","ideographic")
            .attr("class",useClass)
            .attr("dx","-0.1em")
            .attr("dy","-0.3em")
            .style("text-anchor", "middle")
            .attr("dy","1.1em")
            .attr("x",this.x(this.max_x))
            .attr("y",function(d) {
                //var retVal = y(eg.minYDomain);
                var retVal = eg.height;
                return retVal;
            })
            .text(hl_utils.getTranslationLabel("max. %(val)s",hl_utils.formatMilliseconds(eg.maxXDomain)));

    },
    addDateLabels:function(data,first_val,last_val)
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        var first_baseline = "hanging";
        var last_baseline = "hanging";

        var useClass = "date-start";
        var useId = useClass+"_label_"+this.group_id;

        useClass = "date-max";
        useId = useClass+"_label_"+this.group_id;
        eg.focus.append("text")
            .attr("id", useId)
            //.attr("dominant-baseline","ideographic")
            .attr("class",useClass)
            .attr("dx","-0.1em")
            .attr("dy","-0.3em")
            .style("text-anchor", "middle")
            .attr("dy","1.1em")
            .attr("x",this.x(this.max_x))
            .attr("y",function(d) {
                var retVal = eg.height;
                return retVal;
            })
            .text(hl_utils.getTranslationLabel("max. %(val)s",hl_utils.formatMilliseconds(eg.maxXDomain)));

    },
    getPriceMarkerEnd:function(i,data)
    {
        var marker_end = "";
        if(i == data.length-2)
        {
            var event = data[i+1];
            var markerId = this.markerMap.arrow;
            if(event.event_type === 'deactivationprice')
            {
                markerId = this.markerMap.stub;
            }
            marker_end = "url(#"+markerId+")";
        }
        return marker_end;
    },
    addPriceLine:function(data)
    {
        var eg = this;
        var x = this.x;
        var y = this.y;
        this.data = data;
        var last_val = -1;
        var first_val = -1;
        var lastIndex = 0;
        var group_g = this.focus.append("g")
            .attr('class','group-path');
        for(var i=0;i<data.length-1;i++)
        {
            if(first_val < 0)
            {
                first_val = data[i];
            }
            var useData = [data[i],data[i+1]];
            var marker_end = this.getPriceMarkerEnd(i,data);
             var path = group_g.append("path")
              .datum(useData)
              .attr("id", "price_"+i)
              .attr("class", function(d){
                  var retVal = "line price";
                  //////////console.info("d['parent_status_x'] = "+d[0]['parent_status_x']);
                  if(d[0].hasOwnProperty("parent_status_x") && d[0].parent_status_x !== "")
                  {
                      retVal+= " status-"+d[0].parent_status_x;
                  }
                  return retVal;
                })
              .attr("marker-end", marker_end)
              .attr("d", this.line);

              last_val = data[i+1];
              lastIndex = i+1;

        }
        this.addPriceLabels(data,first_val,last_val);
    }

};

function OverviewGraph(containerSelector,analytics_obj){
    this.containerSelector = containerSelector;
    this.analytics_obj = analytics_obj;
    this.group_id = "overview_graph";
    setGraphInitFromAnalytics(this,analytics_obj);
    this.defaultWidth = 400;
    this.defaultMinHeight = 300;

    this.leftMargin = 130;
    this.rightMargin = 80;
    this.topMargin = 40;
    this.bottomMargin = 60;
    this.xAxisLabelDy = "2.8em";
}
OverviewGraph.prototype = Object.create(EventsGraph.prototype);
var overviewGraphPrototype = {
    constructor: OverviewGraph,
    getMedianSeries: function() {
        var lineMargin = 8;
        var _this = this;
        var series = [];
        var groupDataArr = [];
        groupDataArr[0] = {};
        groupDataArr[0][_this.xKey] = this.median_x;
        groupDataArr[0][_this.yKey] = _this.minYDomain-lineMargin;

        groupDataArr[1] = {};
        groupDataArr[1][_this.xKey] = this.median_x;
        groupDataArr[1][_this.yKey] = _this.median_y;

        series.push(groupDataArr);
        return series;
    },
    getMaxYDomain:function()
    {
        return this.max_y+this.max_y*0.05;
    },
    getPriceMarkerEnd:function(i,data)
    {
        //override marker end so that arrows don't show in overview graph
        var marker_end = "";
        return marker_end;
    },
    drawAxisArrows:function(xa,ya)
    {
        var xMarkerId = this.markerMap.arrow;
        var yMarkerId = this.markerMap.uparrow;

        xa.select("path").attr("marker-end", "url(#"+xMarkerId+")");
        ya.select("path").attr("marker-start", "url(#"+yMarkerId+")");
    },
    drawAnalyticsLines:function()
    {

        var eg = this;
        var useArr = eg.analyticsYLinesArr;
        //the price Av line(s)
        var analyticsMargin = 0.03;
        var i;
        var xLineKey;
        if(useArr instanceof Array)
        {
            for(i=0;i<useArr.length;i++)
            {
                var lineKey = useArr[i];
                var lineVal = eg.analytics_obj[lineKey];
                xLineKey = eg.analyticsXLinesArr[i];
                var max_x = eg.analytics_obj[xLineKey];
                //eg.drawAnalyticsLine(lineKey,[eg.min_x,lineVal,eg.max_x,lineVal]);
                eg.drawAnalyticsLine(lineKey,[eg.min_x-analyticsMargin*eg.min_x,lineVal,max_x,lineVal]);
                eg.addPriceAverageLabel(lineKey,eg.min_x,lineVal);

            }
        }

        useArr = eg.analyticsXLinesArr;
        //the date Av line(s)
        if(useArr instanceof Array)
        {
            for(i=0;i<useArr.length;i++)
            {
                xLineKey = useArr[i];
                var yLineKey = eg.analyticsYLinesArr[i];

                var xLineVal = eg.analytics_obj[xLineKey];
                var yLineVal = eg.analytics_obj[yLineKey];

                eg.drawAnalyticsLine(yLineKey,[xLineVal,eg.minYDomain-analyticsMargin*eg.minYDomain,xLineVal,yLineVal]);
                eg.addDateAverageLabel(xLineVal);
            }
        }
    }
};
$.extend(OverviewGraph.prototype,overviewGraphPrototype);
function setAnalyticsGraphInitFromAnalytics(_obj,analytics_obj)
{
    _obj.xKey = 'asking_price';
    _obj.yKey = 'asking_price_height';

    if(analytics_obj)
    {
        _obj.median_x = analytics_obj[hl.AnalyticsTypes.MEDIAN_ASKINGPRICE];
        _obj.mean_x = analytics_obj[hl.AnalyticsTypes.MEAN_ASKINGPRICE];
        _obj.min_x = analytics_obj[hl.AnalyticsTypes.MIN_ASKINGPRICE];
        _obj.max_x = analytics_obj[hl.AnalyticsTypes.MAX_ASKINGPRICE];
    }

}
function AnalyticsPriceDistribtion(containerSelector,similar_properties_obj,analytics_obj){

    this.containerSelector = containerSelector;
    this.similar_properties_obj = similar_properties_obj;

    setAnalyticsGraphInitFromAnalytics(this,analytics_obj);
    //if the container width is too small don't have big margins
    if($(this.containerSelector).width() < 300)
    {
        this.leftMargin = 10;
        this.rightMargin = 10;
        this.topMargin = 40;
        this.bottomMargin = 40;
    }
    else
    {
        this.leftMargin = 80;
        this.rightMargin = 80;
        this.topMargin = 40;
        this.bottomMargin = 40;
    }

}
AnalyticsPriceDistribtion.prototype = Object.create(EventsGraph.prototype);
var analyticsPriceDistribtionPrototype = {
    constructor: AnalyticsPriceDistribtion,
    setWidthAndHeight:function(){

        this.containerWidth = $(this.containerSelector).width() || 800;
        this.containerHeight = $(this.containerSelector).parent().height() || 150;

        var maxHeight = 160;
        this.containerHeight = Math.min(this.containerHeight,maxHeight);

        //this needs to be big enough for the price to show
        this.focusMargin = {top: this.topMargin, right: this.rightMargin, bottom: this.bottomMargin, left: this.leftMargin};
        this.margin = this.focusMargin;

        this.focusHeight = this.containerHeight-this.focusMargin.top-this.focusMargin.bottom;
        this.width = this.containerWidth - this.margin.left - this.margin.right;
        this.height = this.containerHeight - this.margin.top - this.margin.bottom;
    },
    initGraph:function() {
        this.setWidthAndHeight();
        this.setAxisDomains();
        var svgWidth = this.width + this.margin.left + this.margin.right;
        var svgHeight = this.height + this.margin.top + this.margin.bottom;
        //////////console.info("initGraph this.containerSelector = "+this.containerSelector);
        d3.select(this.containerSelector).select('svg').remove();
        var svg = d3.select(this.containerSelector).append("svg")
            .attr("width", svgWidth)
            .attr("height", svgHeight);

        var focus = svg.append("g")
                .attr('id','analytics_focus')
                .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
        this.svg = svg;
        this.focus = focus;
    },
    drawRectangleBackground:function() {
        var _this = this;
        //console.debug("drawFromJson drawMaxMinArea");
         var line = d3.svg.line()
          .x(function(d) {
            return d.x;
          })
          .y(function(d) {
            return d.y;
          });

        var max_min_perc = 0.9;

        var percDiff = 0.9*(this.min_x/this.max_x);
        //////////console.info("this.min_x = "+this.min_x+" this.max_x = "+this.max_x+" percDiff = "+percDiff);
        _this.max_y = this.height;
        _this.min_y = 0;
        var points = [{
            x: _this.x(this.min_x), y: this.height-this.height*percDiff
          },{
            x: _this.x(this.max_x), y: 0
          },{
            x: _this.x(this.max_x), y: this.height
          },{
            x: _this.x(this.min_x), y: this.height
          }];

        this.focus.append('g')
            .attr("class", "distribution-background-rect")
            .append('path')
              .attr("d", line(points) + 'Z')
              ;
        this.focus.append('clipPath')
            .attr("id", _this.getLinesClipPathId())
            .append('path')
              .attr("d", line(points) + 'Z')
              ;
    },
    getAllGroupsObj:function()
    {
        var _this = this;
        if(!this.allGroupsObj)
        {
            var allGroupsObj = {};
            var active_groupid_events_dict = this.similar_properties_obj[hl.SimilarPropertiesTypes.ACTIVE_GROUPID_EVENTS_DICT];
            var inactive_groupid_events_dict = this.similar_properties_obj[hl.SimilarPropertiesTypes.INACTIVE_GROUPID_EVENTS_DICT];
            $.extend(allGroupsObj,active_groupid_events_dict);
            $.extend(allGroupsObj,inactive_groupid_events_dict);
            this.allGroupsObj = allGroupsObj;
        }
        return this.allGroupsObj;
    },
    getPriceDataSeries:function()
    {
        var _this = this;
        var allGroupsObj = this.getAllGroupsObj();
        //from the example here: http://stackoverflow.com/questions/8689498/drawing-multiple-lines-in-d3-js
        var series = [];
        for(var group_id in allGroupsObj)
        {
            var group_obj = allGroupsObj[group_id];
            var groupDataArr = [];
            groupDataArr[0] = {};
            groupDataArr[0][_this.xKey] = group_obj[hl.ReportTypes.ASKING_PRICE];
            groupDataArr[0][_this.yKey] = 0;
            groupDataArr[0][hl.ReportTypes.ACTIVE_STATUS] = group_obj[hl.ReportTypes.ACTIVE_STATUS];
            groupDataArr[0].group_id = group_id;

            groupDataArr[1] = {};
            groupDataArr[1][_this.xKey] = group_obj[hl.ReportTypes.ASKING_PRICE];
            groupDataArr[1][_this.yKey] = _this.height;
            groupDataArr[1][hl.ReportTypes.ACTIVE_STATUS] = group_obj[hl.ReportTypes.ACTIVE_STATUS];
            groupDataArr[1].group_id = group_id;

            series.push(groupDataArr);
        }
        series.sort(function(a, b) {
            var retVal = a[0][hl.ReportTypes.ASKING_PRICE] - b[0][hl.ReportTypes.ASKING_PRICE];
            return retVal;
        });
        return series;
    },
    drawMinMaxPriceLabels:function() {
        var _this = this;
        var series = this.getPriceDataSeries();

        var minPriceDatum = series[0][0];
        var maxPriceDatum = series[series.length-1][0];

        var labelsContainer = this.focus.append("g")
            .attr("class","price-labels-container")
            .attr("x", 0)
            .attr("y", 0)
            .attr("transform", "translate(0," + this.height + ")")
            ;

        labelsContainer.append("text")
          .attr("y", 0)
          .attr("x", _this.x(minPriceDatum[_this.xKey]))
          .attr("dy", "1.4em")
          .style("text-anchor", "middle")
          .text(hl_utils.getTranslationLabel("Prix_Minimum"))
              .append('tspan')
                  .attr('class','maximum')
                  .text(hl_utils.getTranslationLabel(" %(val)s",formatCurrency(minPriceDatum[_this.xKey])));


        labelsContainer.append("text")
          .attr("y", 0)
          .attr("x", _this.x(maxPriceDatum[_this.xKey]))
          .attr("dy", "1.4em")
          .style("text-anchor", "middle")
          .text(hl_utils.getTranslationLabel("Prix_Maximum"))
              .append('tspan')
                  .attr('class','maximum')
                  .text(hl_utils.getTranslationLabel(" %(val)s",formatCurrency(maxPriceDatum[_this.xKey])));


    },
    getMedianSeries: function() {
        var lineMargin = 8;
        var _this = this;
        var series = [];
        var groupDataArr = [];
        groupDataArr[0] = {};
        groupDataArr[0][_this.xKey] = this.median_x;
        groupDataArr[0][_this.yKey] = -lineMargin;

        groupDataArr[1] = {};
        groupDataArr[1][_this.xKey] = this.median_x;
        groupDataArr[1][_this.yKey] = _this.height+lineMargin;

        series.push(groupDataArr);
        return series;
    },
    drawMedianLabels: function() {
        var _this = this;
        var series = this.getMedianSeries();

        var medianPriceDatum = series[0][0];

        var labelsContainer = this.focus.append("g")
            .attr("class","price--median-labels-container")
            .attr("x", 0)
            .attr("y", 0)
            ;

        labelsContainer.append("text")
          .attr("y", 0)
          .attr("x", _this.x(medianPriceDatum[_this.xKey]))
          .attr("dy", "-1em")
          .style("text-anchor", "middle")
          .text(hl_utils.getTranslationLabel("Median"))
              .append('tspan')
                  .attr('class','median')
                  .text(hl_utils.getTranslationLabel(" %(val)s",formatCurrency(medianPriceDatum[_this.xKey])));


    },
    drawMedianPriceLine: function() {
        var _this = this;
        var linesContainer = this.focus.append("g")
            .attr("class","price-median-container")
            .attr("x", 0)
            .attr("y", 0)
            ;


        var series = this.getMedianSeries();

        var line = d3.svg.line()
        //.interpolate("basis")
        .x(function(d) { return _this.x(d[_this.xKey]); })
        .y(function(d) { return d[_this.yKey]; });


        linesContainer.selectAll(".line")
            .data(series)
          .enter().append("path")
            .attr("class", function(d) { return "line median";} )
            .attr("d", line);
    },
    getLinesClipPathId:function()
    {
        return $(this.containerSelector).attr('id')+"_"+"lines_clip_path";
    },
    drawDistributionPriceLines:function() {
        var _this = this;
        var linesContainer = this.focus.append("g")
            .attr("class","price-line-container")
            .attr("clip-path","url(#"+_this.getLinesClipPathId()+")")
            .attr("x", 0)
            .attr("y", 0);

        var series = this.getPriceDataSeries();

        var line = d3.svg.line()
        //.interpolate("basis")
        .x(function(d) { return _this.x(d[_this.xKey]); })
        .y(function(d) { return d[_this.yKey]; });

        linesContainer.selectAll(".line")
            .data(series)
          .enter()
            .append("a")
            .attr("xlink:href", function(d){ return "#similar_property_row_"+d[0].group_id;})
            .on("mouseover", function() {
                d3.select(this).classed("hover", true);
              })
            .on("mouseout", function() {
                 d3.select(this).classed("hover", false);
            })
            .append("path")
            .attr("class", function(d) { return "line "+d[0][hl.ReportTypes.ACTIVE_STATUS];} )
            .attr("d", line);

    }
};
$.extend(AnalyticsPriceDistribtion.prototype,analyticsPriceDistribtionPrototype);
