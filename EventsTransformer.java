package houser.solr.transformer;

import java.util.Map;
import java.util.Date;
import java.util.HashMap;
//import java.util.logging.Logger;
//import java.util.logging.Level;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;
import java.math.BigDecimal;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import houser.solr.transformer.HouserTypes;
import houser.solr.transformer.HouserTransformer;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Collections;
import houser.solr.utils.MapComparator;
import com.google.gson.Gson;
import java.util.OptionalDouble;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.solr.handler.dataimport.JdbcDataSource;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.TimeZone;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Arrays;

/*
 * Transforms and consolidates information from each event belonging to a parent property object
 * cleaning data as we go and adding extra meta-information to the parent row in the process
 * the parent row orig_<fieldname> fields are populated populated with _good_ data from 
 * the consolidated event rows, so that the parent eventually has orig_<fieldname> AND <fieldname> fields.
 * A local memory hashmap is used to store data between rows - this hashmap gets populated and updated
 * as each row is processed until after the last event row is processed when it is written to the 
 * parent row object
 * */
public class EventsTransformer extends HousesDataCleanTransformer{
    
    public static long ONE_MINUTE_IN_MILLIS=60000; //millisecs
    public static int maxStringByteLength=32766;
    private Connection connection;
    
    /* 
     * compare the value in the memMap with the input value, and if it isn't equal, create a compare_key entry in the 
     * memMap. Also update the latest changed date in the memMap if the date of this value change is after the memMap value
     * @param memMap The 'memory' of the parent row which persists between event row processing
     * @param compare_key The key to compare the value of in the memMap
     * @param compare_val The value to compare with the value in the memMap
     * @param moddate The date that the event was modified
     * @param biggest_key_date The latest update date in the memMap for an event change
     * @returns void
     * */
    private void setCompareRow(Map<String, Object> memMap,String compare_key,double compare_val,Date moddate,String biggest_key_date)
    {
        try 
        {
            if(memMap.containsKey(compare_key) && memMap.get(compare_key) != null)
            {
                double absVal = Math.abs((double)memMap.get(compare_key));
                if(absVal < Math.abs((double)compare_val))
                {
                    memMap.put(compare_key, compare_val);
                    memMap.put("abs_"+compare_key, absVal);
                    if(biggest_key_date != null && moddate != null)
                    {
                        memMap.put(biggest_key_date, moddate);
                    }
                }
            }
            else
            {
                if(compare_key != null)
                {
                    memMap.put(compare_key,compare_val);
                    memMap.put("abs_"+compare_key, Math.abs((double)compare_val));
                }
                if(biggest_key_date != null && moddate != null)
                {
                    memMap.put(biggest_key_date, moddate);
                }
            }
        }
        catch (Exception e) 
        {
            doLog(ERROR_LEVEL,"#### setCompareRow memMap="+memMap+" compare_key="+compare_key+" compare_val="+compare_val+" moddate="+moddate+" biggest_key_date="+biggest_key_date+" e="+e);
        } 
        
    }
    
    /*
     * casts the value in the row to a long and returns it, if present, otherwises defaults return to -1
     * */
    private long getLongKey(Map<String, Object> row,String longKey)
    {
        long retVal = -1;
        Object retValObj = row.get(longKey);
        if(retValObj != null)
        {
            retVal=getLongFromObject(retValObj);
        }
        return retVal;
    }
    
    /*
     * Set the latest good price in the memMap if the price is valid - i.e. greater than 0
     * If there is no firstprice set, then set it to the input value
     * */
    private long setLatestGoodPrice(String priceKey,Map<String, Object> row,Map<String, Object> memMap,long orig_price)
    {
        try 
        {
            Object usePriceObj = row.get(priceKey);
            if(usePriceObj != null)
            {
                long usePrice=getLongFromObject(usePriceObj);
                if(usePrice > 0)
                {
                    if(!memMap.containsKey("firstprice"))
                    {
                        memMap.put("firstprice",usePrice);
                    }
                    memMap.put("latest_goodprice",usePrice);
                    memMap.put("price",usePrice);
                    return usePrice;
                }
            }
        }
        catch (Exception e) 
        {
            doLog(ERROR_LEVEL,"#### setLatestGoodPrice priceKey="+priceKey+" row="+row+" memMap="+memMap+" e="+e);
        } 
        long latest_goodprice = getLatestGoodPrice(memMap);
        return latest_goodprice;
    }
    
    /*
     * Update all the row price meta information fields with null
     * */
    private void clearPriceChanges(Map<String, Object> memMap)
    {
        ////////doLog(LEVEL,"#### clearPriceChanges ####");
        memMap.put("latest_perchangeprice", null);
        memMap.put("latest_perchangepricedate", null);
        memMap.put("biggest_perchangeprice", null);
        memMap.put("biggest_pricediff", null);
        memMap.put("abs_biggest_perchangeprice", null);
        memMap.put("abs_biggest_pricediff", null);
        memMap.put("biggest_pricediff_date", null);
        memMap.put("latest_pricediff", null);
        memMap.put("latest_pricediff_date", null);
        memMap.put("changeprice", null);
        memMap.put("pricechange_count", null);
        memMap.put("latest_changedate", null);
        memMap.put("prevvalprice", null);
        memMap.put("total_perpricechange",null);
        memMap.put("total_pricechange",null);
        memMap.put("biggest_perchangeprice_date",null);
        
    }
    
    /*
     * Uses the latest_goodprice to determine whether the parent property object row should be to rent or to buy
     * using the sale_rent_threshold
     * if this is the first row, add the first_sale_rent value to the memMap
     * Otherwise, use the current value of sale_rent from the memMap to compare with the threshold
     * */
    public void setSaleRentDiff(Map<String, Object> row,Context context,long latest_goodprice)
    {
        try
        {
            if(latest_goodprice > 0)
            {
                String origSaleRent = (String)getParentVal(context,row,HouserTypes.ORIG_SALE_RENT);
                if(!memMap.containsKey(HouserTypes.ORIG_SALE_RENT) && origSaleRent != null && !origSaleRent.equals(""))
                {
                    memMap.put(HouserTypes.FIRST_SALE_RENT,origSaleRent);
                }
                String existingSaleRent = (String)memMap.get(HouserTypes.SALE_RENT);
                if(existingSaleRent == null)
                {
                    existingSaleRent = origSaleRent;
                }
                
                if(!memMap.containsKey(HouserTypes.SALE_RENT))
                {
                    memMap.put(HouserTypes.SALE_RENT,existingSaleRent);
                }
                if(existingSaleRent != null)
                {
                    String useSalerent = "";
                    if(latest_goodprice > HouserTypes.SALERENT_THRESHOLD)
                    {
                        if(existingSaleRent.compareTo(HouserTypes.NEW)==0)
                        {
                            useSalerent = HouserTypes.NEW;
                        }
                        else
                        {
                            useSalerent = HouserTypes.BUY;
                        }
                    }
                    else
                    {
                        useSalerent = HouserTypes.RENT;
                    }
                    
                    String firstSaleRent = (String)memMap.get(HouserTypes.FIRST_SALE_RENT);
                    String prevSaleRent = null;
                    if(memMap.containsKey(HouserTypes.PREV_SALE_RENT))
                    {
                        prevSaleRent = (String)memMap.get(HouserTypes.PREV_SALE_RENT);
                    }
                    
                    if(useSalerent != null)
                    {
                        memMap.put(HouserTypes.PREV_SALE_RENT,useSalerent);
                    }
                }
                
            }
        }
        catch (Exception e) 
        {
            doLog(ERROR_LEVEL,"#### setSaleRentDiff row="+row+" latest_goodprice="+latest_goodprice+" e="+e);
        } 
    }
    
    public void setMapKeyIfExists(Map nl,String key,Object val)
    {
          nl.put(key,val);
    }
    private long getLatestGoodPrice(Map<String, Object> memMap)
    {
        long latest_goodprice = -1;
        if(memMap.containsKey("latest_goodprice"))
        {
            latest_goodprice = (long)memMap.get("latest_goodprice");
        }
        return latest_goodprice;
    }
    private boolean getBoolValueFromMap(Map<String, Object> memMap,String key,boolean defaultValue)
    {
        boolean latest_val = defaultValue;
        if(memMap.containsKey(key))
        {
            latest_val = (boolean)memMap.get(key);
        }
        return latest_val;
    }
    private void updateIfGoodLong(Map<String, Object> memMap,String mapKey, long val)
    {
        if(val > 0)
        {
            memMap.put(mapKey,val);
        }
    }
    
    /*
     * if the current event is newer than the currently stored value in memMap then update
     * the latest_event as well as the latest_event_type
     * */
    private void doUpdateLatestEventData(Map<String, Object> memMap,Date moddate,String eventType,boolean override)
    {
        Date existingEventChangeDate;
        String dateKey = "latest_event_date";
        String eventTypeKey = "latest_event_type";
        updateLatestDate(memMap,eventType,moddate);
        if(moddate != null && dateKey != null)
        {
            if(memMap.containsKey(dateKey) && memMap.get(dateKey) != null)
            {
                existingEventChangeDate = (Date)memMap.get(dateKey);
                if(moddate.compareTo(existingEventChangeDate) > 0 || override)
                {
                    memMap.put(dateKey,moddate);
                    memMap.put(eventTypeKey,eventType);
                }
            }
            else
            {
                memMap.put(dateKey,moddate);
                memMap.put(eventTypeKey,eventType);
            }
        }
    }
    private void updateLatestEventData(Map<String, Object> memMap,Date moddate,String eventType)
    {
        doUpdateLatestEventData(memMap,moddate,eventType,true);
    }
    private void updateLatestEventData(Map<String, Object> memMap,Date moddate,String eventType,boolean override)
    {
        doUpdateLatestEventData(memMap,moddate,eventType,override);
    }
    private void updateLatestDate(Map<String, Object> memMap,String dateKey,Date moddate)
    {
        Date existingChangeDate;
        if(moddate != null && dateKey != null)
        {
            if(memMap.containsKey(dateKey) && memMap.get(dateKey) != null)
            {
                existingChangeDate = (Date)memMap.get(dateKey);
                if(moddate != null && moddate.compareTo(existingChangeDate) > 0 )
                {
                    memMap.put(dateKey,moddate);
                }
            }
            else
            {
                memMap.put(dateKey,moddate);
            }
        }
    }
    protected ArrayList<Map<String, Object>> setListOrderByName(ArrayList<Map<String, Object>> menuItems2,String dateKey) 
    {
        Collections.sort(menuItems2, new MapComparator(dateKey));
        return menuItems2;
    } 
    
    /*
     * Looks for specific case to fix a data collection quirk where a created event occurs just before an activation or deactivation event. If this is the case,
     * set the moddate of the current event to before the next event's created date so that the moddate ordering becomes
     * consistent
     * Sort the array by moddate once all corrections have been made
     * */
    public ArrayList<Map<String,Object>> reorder_activation_events(ArrayList<Map<String,Object>> results,Map<String, Object> memMap)
    {
        
        int results_len = results.size();
        
        for(int i=0;i<=results_len;i++)
        {
            if(i+1 < results_len)
            {
                Map<String,Object> entry = (Map)results.get(i);
                Map<String,Object> next_entry = (Map)results.get(i+1);
                Integer entryUid = (Integer)entry.get("uid");
                Integer nextEntryUid = (Integer)next_entry.get("uid");
                if((entryUid.intValue() == nextEntryUid.intValue() ) && (next_entry.containsKey("activationprice") || entry.containsKey("activationprice") ||  next_entry.containsKey("deactivationprice") || entry.containsKey("deactivationprice")) )
                {
                    Date nextCreatedDate = (Date)next_entry.get("created");
                    Date entryCreatedDate = (Date)entry.get("created");
                    if(nextCreatedDate.compareTo(entryCreatedDate) < 0 && !(next_entry.containsKey("createdprice") && next_entry.get("createdprice") != null))
                    {
                        
                        Date mod_date = (Date)entry.get("moddate");
                        long t = mod_date.getTime();
                        
                        mod_date = new Date(t - (30 * EventsTransformer.ONE_MINUTE_IN_MILLIS));
                        Date orig_next_entry_mod_date = (Date)next_entry.get("moddate");
                        memMap.put("event_moddate_changed",true);
                        memMap.put("orig_moddate",orig_next_entry_mod_date);
                        next_entry.put("moddate",mod_date);
                    }
                }
            }
        }
        ArrayList newlist = setListOrderByName(results,"moddate");
        return newlist;
    }
    
    /*
     * set last and latest activation dates in the memMap, using the active boolean to 
     * determine if the parent was previously inactive and is thus being activated
     * */
    protected void setLastAactivationDates(Map<String, Object> memMap)
    {
        
        boolean isActive = getBoolValueFromMap(memMap,"active",true);
        //////doLog(LEVEL,"#### setLastAactivationDate isActive = "+isActive);
        if(isActive == false)
        {
            Date latest_deactivationdate = getDateFromKey("latest_deactivationdate");
            if(latest_deactivationdate != null)
            {
                memMap.put("last_deactivationdate",latest_deactivationdate);
                memMap.put("last_activationdate",null);
            }
            else
            {
                memMap.put("last_deactivationdate",null);
            }
        }
        else
        {
            Date latest_activationdate = getDateFromKey("latest_activationdate");
            if(latest_activationdate == null)
            {
                latest_activationdate = getDateFromKey("latest_createddate");
            }
            //////doLog(LEVEL,"#### setLastAactivationDates latest_activationdate="+latest_activationdate);
            memMap.put("last_activationdate",latest_activationdate);
            memMap.put("last_deactivationdate",null);
        }
    }
    public Timestamp getUTCTimestamp()
    {
         Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
         
        // get a java.util.Date from the calendar instance.
        //  this date will represent the current instant, or "now".
        java.util.Date now = calendar.getTime();
         
        // a java current time (now) instance
        Timestamp last_modified = new Timestamp(now.getTime());
        return last_modified;
    }
    
    /*
     * write the dup_id in the database to null in both the houses and duke_dups tables
     * */
    protected int clearDupId(Integer uid,String group_id,Context context,Map<String, Object> memMap)
    {
        int housesUpdated = 0;
        Map<String,Object> requestParameters = context.getRequestParameters();
        ////doLog(LEVEL,"#### clearDupId requestParameters = "+requestParameters);
        
        String db_ip = (String)requestParameters.get("db_write_ip");
        String db_name = (String)requestParameters.get("db_write_name");
        String db_user_name = (String)requestParameters.get("db_write_user");
        String db_user_password = (String)requestParameters.get("db_write_password");
        
        
        String driverStr = "org.postgresql.Driver";
        String connectionStr = "jdbc:postgresql://"+db_ip+"/"+db_name;
        ////doLog(LEVEL,"#### clearDupId connectionStr = "+connectionStr);
        Connection connection = null;
        try
        {
            Class.forName(driverStr);
            ////doLog(LEVEL,"#### clearDupId after Class.forName(driverStr)");
            Properties props = new Properties();
            props.setProperty("user",db_user_name);
            props.setProperty("password",db_user_password);
            props.setProperty("ssl","true");
            props.setProperty("sslfactory","org.postgresql.ssl.NonValidatingFactory");
            connection = DriverManager
                            .getConnection(connectionStr,props);
            connection.setAutoCommit(false);
            ////doLog(LEVEL,"#### clearDupId connection created successfully connection = "+connection);
        } catch (Exception e) {
            doLog(ERROR_LEVEL,"#### clearDupId e = "+e);
        }
        if(connection != null)
        {
            try {
                
                  PreparedStatement updateHouses = connection.prepareStatement("UPDATE houses SET dup_id = NULL,bad_dup_id = ?,bad_dup_last_modified = ? where dup_id=?;");
                  int group_id_int = Integer.parseInt(group_id);
                  updateHouses.setInt(1, group_id_int);
                  
                  Timestamp last_modified = getUTCTimestamp();
                  updateHouses.setTimestamp(2, last_modified);
                  
                  updateHouses.setInt(3, group_id_int);
                  
                  housesUpdated = updateHouses.executeUpdate();
                  ////doLog(LEVEL,"#### clearDupId housesUpdated = "+housesUpdated);
                  PreparedStatement updateDukeDups = connection.prepareStatement("UPDATE duke_dups SET bad_dup=true WHERE dup_id=?");
                  updateDukeDups.setInt(1, group_id_int);
                  int dukeDupsUpdated = updateDukeDups.executeUpdate();
                  ////doLog(LEVEL,"#### clearDupId dukeDupsUpdated = "+dukeDupsUpdated);
                connection.commit();
              }
              catch(SQLException ex) {
                  doLog(ERROR_LEVEL,"#### clearDupId ex = "+ex);
              }
          }
          return housesUpdated;
    }
    
    /*
     * special handling for last event row: it's time to set total meta values for the parent row
     * so some calculations for price per m2 and price per are can be performed
     * we can also detect 'bad' duplicates using the number of price oscillations the property has had
     * more than 2 is a sign that it has been falsely labelled as a duplicate
     * */
    private Map<String, Object> handleLastRow(Integer uid,Map<String, Object> row,Context context,Map<String, Object> memMap)
    {
        
         //doLog(LEVEL,"#### handleLastRow ");
        /*
        Map<String, Object> memMap = (Map<String, Object>)context.getSessionAttribute("memMap","entity");
        
        * */
        long parent_uid = getLongValueFromContext(context,"houses.uid_str",uid);
        ////doLog(LEVEL,"#### handleLastRow parent_uid = "+parent_uid);
        if(memMap == null)
        {
            memMap = new HashMap<String, Object>();
        }
        boolean doClearPriceChanges = false;
        if(memMap.containsKey("doClearPriceChanges") && (boolean)memMap.get("doClearPriceChanges"))
        {
            doClearPriceChanges = true;
        }
        boolean saleRentChanged = false;
        if(memMap.containsKey(HouserTypes.SALE_RENT_CHANGED))
        {
            saleRentChanged = true;
        }
        long latest_goodprice = -1;
        long orig_price = getLongValueFromContext(context,"houses.orig_price",uid);
        //////////doLog(LEVEL,"#### total_perpricechange memMap.containsKey(firstprice) = "+memMap.containsKey("firstprice")+"  && memMap.containsKey(latest_goodprice)="+memMap.containsKey("latest_goodprice")+" && !saleRentChanged="+!saleRentChanged+" && !doClearPriceChanges="+!doClearPriceChanges);
        if(memMap.containsKey("firstprice") && memMap.containsKey("latest_goodprice") && !saleRentChanged && !doClearPriceChanges)
        {
            latest_goodprice = (long)memMap.get("latest_goodprice");
            
            long firstprice = (long)memMap.get("firstprice");
            long price_min = getLongValueFromContext(context,"houses.price_min",uid);
            long price_max = getLongValueFromContext(context,"houses.price_max",uid);
            if(price_min < 0 && price_max < 0)
            {
                long total_pricechange = (latest_goodprice-firstprice);
                float total_perpricechange = (float)total_pricechange/(float)firstprice;
                memMap.put("total_perpricechange",total_perpricechange);
                memMap.put("total_pricechange",total_pricechange);
            }
        }
        long usePrice = getLongFromRow(row,"price");
        if(usePrice > 0 && latest_goodprice < 0)
        {
            memMap.put("latest_goodprice",usePrice);
        }
        if(memMap.containsKey(HouserTypes.SALE_RENT_CHANGED))
        {
            clearPriceChanges(memMap);
        }
        if(!memMap.containsKey("latest_goodprice") && orig_price > 0)
        {
            memMap.put("latest_goodprice",orig_price);
            memMap.put("price",orig_price);
        }
        
        double pricesqm = -1;
        if(memMap.containsKey("price") && (long)memMap.get("price") > 0 && memMap.containsKey("livingsize") && (long)memMap.get("livingsize") > 0)
        {
            pricesqm =  (long)memMap.get("price")/(long)memMap.get("livingsize");
        }
        memMap.put("pricesqm",pricesqm);
        
        double pricesqa = -1;
        if(memMap.containsKey("price") && (long)memMap.get("price") > 0 && memMap.containsKey("groundsize") && (long)memMap.get("groundsize") > 0)
        {
            pricesqa =  (long)memMap.get("price")/(long)memMap.get("groundsize");
            
        }
        memMap.put("pricesqa",pricesqa);
        
        //set last deactivation date with latest deactivation date if it exists
        setLastAactivationDates(memMap);

        boolean bad_dup = false;
        List<Long> latest_goodprice_list = (ArrayList<Long>)memMap.get("latest_goodprice_list");
        
        if(latest_goodprice_list != null && !latest_goodprice_list.isEmpty() && latest_goodprice_list.size() > 1)
        {
            if(latest_goodprice_list.size() > 20)
            {
                latest_goodprice_list = latest_goodprice_list.subList(0, 19);
            }
            memMap.put("latest_goodprice_list",latest_goodprice_list);
        }
        
        
        if(memMap.containsKey("oscillate_price_count"))
        {
            int oscillate_price_count = (int)memMap.get("oscillate_price_count");
            //////doLog(LEVEL,"#### oscillate_price_count = "+oscillate_price_count);////doLog(LEVEL,"#### oscillate_price_count = "+oscillate_price_count);
            memMap.put("oscillate_price_count",oscillate_price_count);
            if(oscillate_price_count >= 3)
            {
                bad_dup = true;
            }
        }
        
        String orig_group_id = getStringValueFromContext(context,"houses.orig_group_id",uid,null);
        //String orig_group_id = (String)row.get("orig_group_id");
        String group_id = orig_group_id;
        if(bad_dup)
        {
            //disabling setting group_id differently as the events won't match
            //group_id = "-"+parent_uid;
            memMap.put("bad_dup",true);
        }
        if(group_id == null)
        {
            group_id = "-"+parent_uid;
        }
        memMap.put("final_group_id",group_id);
        memMap.put("group_id",group_id);
        row.putAll(memMap);   
        
        return row;
    }
    
    private Date getDateFromKey(String dateKey)
    {
        Date ret_date = null;
        if(memMap.containsKey(dateKey))
        {
            ret_date = (Date)memMap.get(dateKey);
        }
        return ret_date;
    }
    
    /*
     * main entry function for transformer
     * 
     * we use the row_count from the parent row to decide when we've processed all the child event rows
     * When it's the last row, we put all the values from the memMap into the row and return it
     * @param row The row represents both the event row AND the parent row, although existing fields in the parent row 
     * cannot be overriden here, only new fields can be added
     * @param context The SOLR context which allows us to store hashmaps to act as a memory between event rows
     * @returns the updated row
     * */
    public Object transformRow(Map<String, Object> row,Context context) 
    {
        Integer uid = (Integer)row.get("uid");
        if( row.containsKey("row_count"))
        {
            long rowCount = (long)row.get("row_count");
            memMap = (Map<String, Object>)context.getSessionAttribute("memMap","entity");
            if(memMap == null)
            {
                memMap = new HashMap<String, Object>();
            }
            memMap.put("row_count",rowCount);

            ArrayList<Map<String, Object>> transformArray = null;
            if(memMap.containsKey("transformArray"))
            {
                transformArray = (ArrayList<Map<String, Object>>)memMap.get("transformArray");
            }
            else
            {
                transformArray = new ArrayList<Map<String, Object>>();
                
            }
            int uidCount = incrementCount("event_uid_count",memMap);
            context.setSessionAttribute("memMap",memMap,"entity");
            transformArray.add(row);
            if(rowCount == uidCount)
            {
                ArrayList<Map<String,Object>> reorderedEventRows = reorder_activation_events(transformArray,memMap);
            
                Map<String, Object> parentMap = new HashMap();
                for(int i=0;i<reorderedEventRows.size();i++)
                {
                    Map<String, Object> useRow = (Map<String, Object>)reorderedEventRows.get(i);
                    row = (Map<String, Object>)transformListRow(useRow,context);
                    if(i == reorderedEventRows.size()-1)
                    {
                        row = handleLastRow(uid,row,context,memMap);
                        Map<String, Object> cleanedRow = doDataCleanRow(row,context,memMap);
                        row.putAll(cleanedRow);
                    }
                }
            }
            else
            {
                memMap.put("transformArray",transformArray);
            }
        }
        return row;
    }
    
    public Object transformListRow(Map<String, Object> row,Context context) {
        
        Integer uid = (Integer)row.get("uid");
        ////doLog(LEVEL,"^^^^^^^^^^^ EventsTransformer transformListRow uid="+uid+"^^^^^^^^^^^");
        
        /*
         * if it's the first event row, initialise the entity memory so data can persist between event row processing
         * */
        memMap = (Map<String, Object>)context.getSessionAttribute("memMap","entity");
        if(memMap == null)
        {
            memMap = new HashMap<String, Object>();
        }
        try 
        {
            /* we need row_count do detect when the last row is
             * */
            if( row.containsKey("row_count"))
            {
                long rowCount = (long)row.get("row_count");
                long statsRowCount = (long)context.getStats().get("rowCount");
                long statsQueryCount = (long)context.getStats().get("queryCount");
                long statsDocCount = (long)context.getStats().get("docCount");
                long latest_goodprice = getLatestGoodPrice(memMap);
                long prev_latestgoodprice = -1;
                long price_min = -1;
                long price_max = -1;
                long orig_price = -1;
                long livingsize = -1;
                long groundsize = -1;
                Date orig_first_seen = null;
                Date orig_last_seen = null;
                Date orig_inserted = null;
                boolean orig_active = true;
                boolean isActive = false;
                boolean activeSet = false;
                if(memMap.containsKey("active"))
                {
                    activeSet = true;
                    isActive = getBoolValueFromMap(memMap,"active",orig_active); //////doLog(LEVEL,"active:"+isActive+" memMap.containsKey('active')");
                }
                if(boolValueExists(context,"houses.orig_active",uid))
                {
                    incrementCount("event_count",memMap);
                }
                /*
                 * use the parent context to get values from the parent row
                 * */
                Context parentContext = context.getParentContext();
                try 
                {
                    if(parentContext != null)
                    {
                        //from http://qnalist.com/questions/644076/getting-value-from-parent-query-in-subquery-transformer
                        price_min = getLongValueFromContext(context,"houses.price_min",uid);
                        price_max = getLongValueFromContext(context,"houses.price_max",uid);
                        orig_active = getBooleanValueFromContext(context,"houses.orig_active",uid,orig_active);
                        if(boolValueExists(context,"houses.orig_active",uid) && !memMap.containsKey("active"))
                        {
                            activeSet = true;
                            isActive = orig_active; 
                        }
                        orig_price = getLongValueFromContext(context,"houses.orig_price",uid);
                        livingsize = getLongValueFromContext(context,"houses.livingsize",uid);
                        groundsize = getLongValueFromContext(context,"houses.ground_size",uid);
                        orig_first_seen = getDateValueFromContext(context,"houses.orig_first_seen",uid);
                        orig_last_seen = getDateValueFromContext(context,"houses.orig_last_seen",uid);
                        orig_inserted = getDateValueFromContext(context,"houses.inserted",uid);
                        updateIfGoodLong(memMap,"livingsize", livingsize);
                        updateIfGoodLong(memMap,"groundsize", groundsize);
                        
                    }
                }
                catch (Exception e) 
                {
                    doLog(ERROR_LEVEL,"#### parentContext getVariableResolver uid="+uid+" e="+e);
                } 
                
                
                Date moddate = (Date)row.get("moddate");
                if(moddate != null)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    //values before 2013-02-28 weren't scraped correctly
                    Date earliestAllowedDate = sdf.parse("2013-02-28");
                    if(earliestAllowedDate.compareTo(moddate) <= 0)
                    {
                        updateDate(memMap,"first_seen",moddate,true);
                        updateDate(memMap,"last_seen",moddate,false);
                    }
                }
                updateDate(memMap,"first_seen",orig_first_seen,true);
                updateDate(memMap,"last_seen",orig_last_seen,false);
                
                String dupStringKey = "dup_uids";
                String e_site = (String)row.get("e_site");
                addDupUid("dup_uids",memMap,uid,e_site);
                
                long prev_latest_goodprice = latest_goodprice;
                latest_goodprice = setLatestGoodPrice("createdprice",row,memMap,orig_price);
                latest_goodprice = setLatestGoodPrice("deactivationprice",row,memMap,orig_price);
                latest_goodprice = setLatestGoodPrice("activationprice",row,memMap,orig_price);
                latest_goodprice = setLatestGoodPrice("changeprice",row,memMap,orig_price);
                
                setSaleRentDiff(row,context,latest_goodprice);
                    
                boolean saleRentChanged = false;
                if(memMap.containsKey(HouserTypes.SALE_RENT_CHANGED))
                {
                    saleRentChanged = true;
                }
                boolean doClearPriceChanges = false;
                
                /*
                 * if the property has a price range set that means it's a building with many apartments or a plot
                 * of land with multiple properties so we shouldn't calculate price diffs for it
                 * */
                if(price_min < 0 && price_max < 0 && !saleRentChanged)
                {
                    boolean doIncrementChangeCount = false;
                    /*
                     * if there has been a price change, then register a price oscillation (if present)
                     * otherwise, calculate and save the price difference, also increment the price change count
                     * */
                    if(memMap.containsKey("prev_latestgoodprice"))
                    {
                        prev_latestgoodprice = (long)memMap.get("prev_latestgoodprice");
                        if((prev_latestgoodprice < latest_goodprice) || (prev_latestgoodprice > latest_goodprice))
                        {
                            long current_price_diff = latest_goodprice-prev_latestgoodprice;
                            if(memMap.containsKey("prev_price_diff"))
                            {
                                long prev_price_diff = (long)memMap.get("prev_price_diff");
                                if((prev_price_diff > 0 && current_price_diff < 0) || (prev_price_diff < 0 && current_price_diff > 0))
                                {
                                    int oscillate_price_count = incrementCount("oscillate_price_count",memMap);
                                }
                            }
                            memMap.put("prev_price_diff",current_price_diff);
                            double perc_price_diff = (100*current_price_diff)/prev_latestgoodprice;
                        }
                    }
                    
                    addToStoredLongArray("latest_goodprice_list",memMap,latest_goodprice,true);
                    long prevvalprice = -1;
                    Object prevvalpriceObj = row.get("prevvalprice");
                    if(prevvalpriceObj != null)
                    {
                        prevvalprice = getLongFromObject(prevvalpriceObj);
                    }
                    long changeprice = -2;
                    Object changepriceObj = row.get("changeprice");
                    try
                    {
                        if(changepriceObj != null)
                        {
                            changeprice = getLongFromObject(changepriceObj);
                        }
                    }
                    catch (Exception e) 
                    {
                        doLog(ERROR_LEVEL,"#### changeprice,pricechange_count uid="+uid+" e="+e);
                    } 
                    
                    boolean changeValid = true;
                    if(changeprice > -2 && changeprice < 1)
                    {
                        changeValid = false;
                    }
                    
                    Object pricediffObj = row.get("pricediff");
                    /*
                     * get the pre-calculated price diff from the event row and update relevant keys in memMap
                     * */
                    try
                    {
                        if(pricediffObj != null)
                        {
                            BigDecimal pricediffObjBD = getBigDecimalFromObject(pricediffObj);
                            double pricediff = pricediffObjBD.doubleValue();
                            if(latest_goodprice > 0 && prev_latestgoodprice > 0 && Math.abs((double)pricediff) > 0 && latest_goodprice != prev_latestgoodprice)
                            {
                                double real_pricediff = latest_goodprice-prev_latestgoodprice;
                                setCompareRow(memMap,"biggest_pricediff",real_pricediff,moddate,"biggest_pricediff_date");
                                memMap.put("latest_pricediff", real_pricediff);
                                memMap.put("latest_pricediff_date", moddate);
                                updateLatestEventData(memMap,moddate,"latest_changedate");
                                doIncrementChangeCount = true;
                            }
                        }
                    }
                    catch (Exception e) 
                    {
                        doLog(ERROR_LEVEL,"#### pricediffObj uid="+uid+" e="+e);
                    } 
                    try
                    {
                        Object perchangepriceObj = row.get("perchangeprice");
                        if(perchangepriceObj != null)
                        {
                            BigDecimal perchangepriceObjBD = getBigDecimalFromObject(perchangepriceObj);
                            double perchangeprice = perchangepriceObjBD.doubleValue();
                            if(latest_goodprice > 0 && prev_latestgoodprice > 0 && Math.abs((double)perchangeprice) > 0 && latest_goodprice != prev_latestgoodprice)
                            {
                                double real_pricediff = latest_goodprice-prev_latestgoodprice;
                                double real_perchangeprice = real_pricediff/prev_latestgoodprice;
                                setCompareRow(memMap,"biggest_perchangeprice",real_perchangeprice,moddate,"biggest_perchangeprice_date");
                                
                                memMap.put("latest_perchangeprice", real_perchangeprice);
                                memMap.put("latest_perchangepricedate", moddate);
                                doIncrementChangeCount = true;
                                updateLatestEventData(memMap,moddate,"latest_changedate");
                            }
                        }
                    }
                    catch (Exception e) 
                    {
                        doLog(ERROR_LEVEL,"#### perchangepriceObj uid="+uid+" e="+e);
                    }
                    if(memMap.containsKey("latest_goodprice"))
                    {
                        latest_goodprice = (long)memMap.get("latest_goodprice");
                    }
                    try 
                    {
                        ////////doLog(LEVEL,"#### uid="+uid+" prev_latestgoodprice="+prev_latestgoodprice+" latest_goodprice = "+latest_goodprice);
                        if(latest_goodprice > 0 && prev_latestgoodprice > 0 && latest_goodprice != prev_latestgoodprice)
                        {
                            double real_pricediff = latest_goodprice-prev_latestgoodprice;
                            double real_perchangeprice = (real_pricediff)/prev_latestgoodprice;
                            setCompareRow(memMap,"biggest_pricediff",real_pricediff,moddate,"biggest_pricediff_date");
                            memMap.put("latest_pricediff", real_pricediff);
                            memMap.put("latest_pricediff_date", moddate);
                            doIncrementChangeCount = true;
                            setCompareRow(memMap,"biggest_perchangeprice",real_perchangeprice,moddate,"biggest_perchangeprice_date");
                            memMap.put("latest_perchangeprice", real_perchangeprice);
                            memMap.put("latest_perchangepricedate", moddate);
                            updateLatestEventData(memMap,moddate,"latest_changedate");
                        } 
                    }
                    catch (Exception e) 
                    {
                        doLog(ERROR_LEVEL,"#### ,latest_pricediff_date uid="+uid+" e="+e);
                    } 
                    if(doIncrementChangeCount == true)
                    {
                        int retCount = incrementCount("pricechange_count",memMap); 
                        updateLatestDate(memMap,"latest_changedate",moddate);
                        
                    }
                }
                else
                {
                    doClearPriceChanges = true;
                    setMapKeyIfExists(memMap,"doClearPriceChanges",true);
                }
                
                if(memMap.containsKey("biggest_perchangeprice") && memMap.get("biggest_perchangeprice") != null)
                {
                    double biggest_perchangeprice = (double)memMap.get("biggest_perchangeprice");
                    if(Math.abs(biggest_perchangeprice) > 0.89)
                    {
                        doClearPriceChanges = true;
                        setMapKeyIfExists(memMap,"doClearPriceChanges",true);
                    }
                    
                }
                if(memMap.containsKey("doClearPriceChanges") && (boolean)memMap.get("doClearPriceChanges"))
                {
                    clearPriceChanges(memMap);
                }
                Date latest_deactivationdate = getDateFromKey("latest_deactivationdate");
                Date latest_activationdate = getDateFromKey("latest_activationdate");
                Date latest_perchangepricedate = getDateFromKey("latest_perchangepricedate");
                
                
                Date deactivationdate = (Date)row.get("deactivationdate");
                Date activationdate = (Date)row.get("activationdate");
                
                long createdprice = getLongKey(row,"createdprice");
                long deactivationprice = getLongKey(row,"deactivationprice");
                long activationprice = getLongKey(row,"activationprice");
                long changeprice = getLongKey(row,"changeprice");
             
                /*
                 * use the moddate, activation and deactivationdate to decide if the property is active or not
                 * */
                if(moddate != null)
                {
                    if(activationdate != null)
                    {
                        isActive = true;
                        incrementCount("activationdate_count",memMap);
                        updateLatestEventData(memMap,activationdate,"latest_activationdate");
                    }
                    if(deactivationdate != null)
                    {
                        
                        incrementCount("deactivationdate_count",memMap);
                        updateLatestEventData(memMap,deactivationdate,"latest_deactivationdate");
                        isActive = false;
                       
                    }
                    if(createdprice > 0)
                    {
                        updateLatestEventData(memMap,moddate,"latest_createddate");
                        isActive = true;//////doLog(LEVEL,"active:true createdprice > 0");
                    }
                    if(activationprice > 0 )
                    {
                        updateLatestEventData(memMap,moddate,"latest_activationdate");
                        isActive = true;//////doLog(LEVEL,"active:true activationprice > 0");
                    }
                    if(changeprice > 0 )
                    {
                        //isActive = true;//////doLog(LEVEL,"active:true");
                        updateLatestEventData(memMap,moddate,"latest_changedate");
                    }
                    if(deactivationprice > 0 )
                    {
                        updateLatestEventData(memMap,moddate,"latest_deactivationdate");
                        isActive = false;//////doLog(LEVEL,"active:false deactivationprice > 0");
                    }
                }
         
                long eventprice = getLongFromRow(row,"eventprice");
                if(eventprice > 0)
                {
                    Date use_latest_activationdate = getDateFromKey("latest_activationdate");
                    if(use_latest_activationdate == null)
                    {
                        use_latest_activationdate = getDateFromKey("latest_createddate");
                    }
                    if( use_latest_activationdate == null)
                    {
                        updateLatestEventData(memMap,moddate,"latest_createddate");
                    }
                }
                if(activeSet)
                {
                    memMap.put("active", isActive);
                }
                if(memMap.containsKey("latest_event_type"))
                {
                    String latest_event_type = (String)memMap.get("latest_event_type");
                }
                
                try
                {
                    int uidCount = incrementCount("event_uid_count",memMap);
                    memMap.put("prev_latestgoodprice", latest_goodprice);
                    context.setSessionAttribute("memMap",memMap,"entity");
                    
                }
                catch (Exception e) 
                {
                    doLog(ERROR_LEVEL,"#### total_perpricechange uid="+uid+" e="+e);
                }
                
            }
            else
            {
                doLog(ERROR_LEVEL,"#### EventsTransformer no row_count for uid="+uid);
                doLog(ERROR_LEVEL,"#### EventsTransformer row.keySet()="+row.keySet());
            }
        } 
        catch (Exception e) {
            doLog(ERROR_LEVEL,"#### transformRow e="+e);
        }    
        
        return row;
    }
    
    public Map<String, Object> doDataCleanRow(Map<String, Object> row,Context context,Map<String, Object> memMap) 
    {
        /*
         * check the sale_rent, price and energy_class for the row and clean it if the values are outside 
         * good tolerances or values
         * */
        try 
        {
            Integer uid = (Integer)row.get("uid");
            ArrayList<String> changedDuringIndexingList = (ArrayList<String>)memMap.get("changedDuringIndexing");
            if(changedDuringIndexingList == null)
            {
                changedDuringIndexingList = new ArrayList<String>();
            }
            if(memMap == null)
            {
                memMap = new HashMap<String, Object>();
            }
            
            int saleRentCleanedCount = cleanSaleRent(row,context,changedDuringIndexingList);
            int cleanPriceOutliersCount = cleanPriceOutliers(row,context,changedDuringIndexingList);
            int cleanEnergyClassCount = cleanEnergyClass(row,context,changedDuringIndexingList);
            
            if(changedDuringIndexingList.size() > 0)
            {
                memMap.put("changedDuringIndexing",changedDuringIndexingList);
            }
            
            String group_id = (String)getParentVal(context,row,"group_id");
            String submitter = (String)getParentVal(context,row,"submitter");
            if(submitter != null)
            {
                String submitter_group_id = submitter+"_"+group_id;
                memMap.put("submitter_group_id",submitter_group_id);
            }
           
            row.putAll(memMap);
        }
        catch (Exception e) 
        {
           doLog(ERROR_LEVEL,"#### doDataCleanRow e="+e);
        } 
        
        return row;
    }
    /*
     * use price to determine whether the sale_rent value should be rent or to buy
     * */
    protected int cleanSaleRent(Map<String, Object> row,Context context,ArrayList<String> changedDuringIndexingList)
    {
        int cleanedCount = 0;
        Integer uid = (Integer)row.get("uid");
        
        String saleRent = (String)getParentVal(context,row,HouserTypes.SALE_RENT);
        String origtype = (String)getParentVal(context,row,HouserTypes.ORIGTYPE);
        
        String origSaleRent = (String)getParentVal(context,row,HouserTypes.ORIG_SALE_RENT);
        if(!memMap.containsKey(HouserTypes.ORIG_SALE_RENT) && origSaleRent != null && !origSaleRent.equals(""))
        {
            memMap.put(HouserTypes.FIRST_SALE_RENT,origSaleRent);
        }
        String existingSaleRent = (String)memMap.get(HouserTypes.SALE_RENT);
        if(existingSaleRent == null)
        {
            existingSaleRent = origSaleRent;
        }
        else
        {
            saleRent = existingSaleRent;
        }
        String useSaleRent = saleRent;
        Object priceObj = row.get("latest_goodprice");
        
        if(priceObj != null)
        {
            long price = getLongFromObject(priceObj);
            if(origtype != null && !origtype.equals("business") && !origtype.equals("garage or parking spot") && !origtype.equals("service") && !origtype.equals("ground") )
            {
                
                if(saleRent.equals(HouserTypes.RENT) && price > HouserTypes.SALERENT_THRESHOLD)
                {
                    cleanedCount += 1;
                    useSaleRent = HouserTypes.BUY;
                    changedDuringIndexingList.add(getChangedString(HouserTypes.SALE_RENT,HouserTypes.RENT,HouserTypes.BUY));
                }
                else if( ( saleRent.equals(HouserTypes.BUY) || saleRent.equals(HouserTypes.NEW) ) && price < HouserTypes.SALERENT_THRESHOLD)
                {
                    cleanedCount += 1;
                    useSaleRent = HouserTypes.RENT;
                    changedDuringIndexingList.add(getChangedString(HouserTypes.SALE_RENT,saleRent,HouserTypes.RENT));
                }
                memMap.put(HouserTypes.SALE_RENT,useSaleRent);
            }
        }
        if(useSaleRent.compareTo(saleRent)!=0 )
        {
            memMap.put(HouserTypes.SALE_RENT_CHANGED,true);
        }
        return cleanedCount;
    }
    
    /*
     * check the energy class against an array of good values and update it to a good value, if present
     * also set a numeric value for energy_class so it can be easily compared
     * */
    protected int cleanEnergyClass(Map<String, Object> row,Context context,ArrayList<String> changedDuringIndexingList)
    {
        int cleanedCount = 0;
        Integer uid = (Integer)row.get("uid");
        String origEnergyClass = (String)getParentVal(context,row,"orig_energy_class");
        
        String useEnergyVal = "";
        String defaultEnergyVal = "";
        if(origEnergyClass != null)
        {
            useEnergyVal = origEnergyClass;
            boolean goodValFound = false;
            for(String str: allowedEnergyValues) {
                if(str.equals(origEnergyClass.trim()))
                {
                    useEnergyVal = str;
                    goodValFound = true;
                    break;
                }
            }
            if(!goodValFound)
            {
                useEnergyVal = defaultEnergyVal;
            }
            if(!useEnergyVal.equals(origEnergyClass))
            {
                cleanedCount += 1;
                changedDuringIndexingList.add(getChangedString(HouserTypes.ENERGY_CLASS,origEnergyClass,useEnergyVal));
            }
        }
        int energy_class_int = allowedEnergyValues.indexOf(useEnergyVal);
        if(energy_class_int >= 0)
        {
            memMap.put(HouserTypes.ENERGY_CLASS_INT,energy_class_int);
        }
        if(!useEnergyVal.equals("") && useEnergyVal != null)
        {
            memMap.put(HouserTypes.ENERGY_CLASS,useEnergyVal);
        }
        return cleanedCount;
    }
    
    /*
     * check that the price for the property is within acceptable limits
     * and that the submitter isn't on a blacklist
     * if so, mark the row as deleted during indexing
     * */
    protected int cleanPriceOutliers(Map<String, Object> row,Context context,ArrayList<String> changedDuringIndexingList)
    {
        int cleanedCount = 0;
        List<String> submitterBlacklist = Arrays.asList("immo123");
        Integer uid = (Integer)row.get("uid");
        String submitter = (String)getParentVal(context,row,"submitter");
       
        if(submitterBlacklist.contains(submitter))
        {
            cleanedCount+= 1;
            changedDuringIndexingList.add(getChangedString("deleted","false","true"));
            memMap.put("deleteDuringIndexing",true);
            memMap.put("$deleteDocById",uid);
        }
        Object priceObj = row.get("latest_goodprice");
        if(priceObj != null)
        {
            long price = getLongFromObject(priceObj);
            if(price >= HouserTypes.PRICE_UPPERLIMIT)
            {
                changedDuringIndexingList.add(getChangedString("deleted","false","true"));
                memMap.put("deleteDuringIndexing",true);
                memMap.put("$deleteDocById",uid);
            }
        }
        return cleanedCount;
    }
    
}
